const path = {
   HOME: "/",
   LOGIN: "/login",
   LOGOUT: "/logout",
   HOUSEHOLD: "/households",
   HOUSEHOLD_DETAIL: "/households/:householdID",
   RESIDENT: "/residents",
   MEETING: "/meetings",
   MEETING_DETAIL: "/meetings/:meetingId",
};
const localStore = {
   USER: "userInfo",
   TOKEN: "access_token",
};
const fieldHousehold = {
   maHoKhau: "Mã hộ khẩu",
   maKhuVuc: "Mã khu vực",
   diaChi: "Địa chỉ",
};
const typeModal = {
   CREATE: 1,
   UPDATE: 2,
};
const genderType = {
   MALE: "Nam",
   FEMALE: "Nữ",
};
const declarationType = {
   TEMPORARY_RESIDENCE: 1,
   TEMPORARY_ABSENT: 2,
   DECLARE_DEATH: 3,
};
export {
   path,
   localStore,
   fieldHousehold,
   typeModal,
   genderType,
   declarationType,
};
