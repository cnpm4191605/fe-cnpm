import moment from "moment";
export const convertDate = (dateTime) => {
   const stringFormat = moment(dateTime).format("DD/MM/YYYY");
   return stringFormat !== "Invalid date" ? stringFormat : "";
};
export const mapDateInput = (dateTime) => {
   const stringFormat = moment(dateTime).format("YYYY-MM-DD");
   return stringFormat !== "Invalid date" ? stringFormat : "";
};
export const convertDateFull = (dateTime) => {
   const stringFormat = moment(dateTime).format("HH:mm DD/MM/YYYY");
   return stringFormat !== "Invalid date" ? stringFormat : "";
};
