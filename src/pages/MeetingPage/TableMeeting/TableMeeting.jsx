import classNames from "classnames/bind";
import style from "./TableMeeting.module.scss";
import { Button, Modal, Table, Form } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toggleModal } from "redux/meeting/action";
import { convertDateFull } from "utils/common";
import { path, typeModal } from "utils/constants";
import { getListMeeting } from "redux/meeting/action";
import { Link } from "react-router-dom";
const cx = classNames.bind(style);

function TableMeeting() {
   const { meetings } = useSelector((state) => state.meeting);
   const dispatch = useDispatch();
   console.log("meetings:", meetings);
   const [showPopup, setShowPopup] = useState(false);
   const [meetingL, setMeetingL] = useState({});
   useEffect(() => {
      dispatch(getListMeeting());
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, []);
   const onRemoveItem = (item) => {
      setShowPopup(true);
      setMeetingL({ ...item });
   };
   const onClose = () => {
      setMeetingL({});
      setShowPopup(false);
   };
   const onSubmit = () => {
      if (JSON.stringify(meetingL) === "{}") {
      } else {
         // dispatch(removeResident(meetingL.id));
      }
      setShowPopup(false);
   };
   console.log("re-render table meeting");
   return (
      <>
         <div className={cx("table-container")}>
            <Table striped bordered hover>
               <thead className={cx("table-header")}>
                  <tr>
                     <th>STT</th>
                     <th>Tên cuộc họp</th>
                     <th>Địa điểm</th>
                     <th>Thời gian bắt đầu</th>
                     <th>Ghi chú</th>
                     <th className={cx("tb-head-action")}>Chức năng</th>
                  </tr>
               </thead>
               <tbody>
                  {meetings &&
                     meetings.length > 0 &&
                     meetings.map((item, index) => (
                        <tr key={item.id}>
                           <td>{index + 1}</td>
                           <td>
                              <Link to={`${path.MEETING}/${item.id}`}>
                                 <b>{item?.title}</b>
                              </Link>
                           </td>
                           <td>{item?.location}</td>

                           <td>
                              {convertDateFull(
                                 item?.startTime ?? "invalid date"
                              )}
                           </td>

                           <td>{item?.notes}</td>
                           <td>
                              <Button
                                 variant="danger"
                                 className={cx("me-2")}
                                 onClick={() => onRemoveItem(item, index)}
                              >
                                 Xóa
                              </Button>
                           </td>
                        </tr>
                     ))}
               </tbody>
            </Table>
         </div>
         <Modal show={showPopup} onHide={onClose} size="md" centered>
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>Xóa cuộc họp</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <p>Tên cuộc họp : {meetingL?.title}</p>
               <p>Địa điểm : {meetingL?.location}</p>
               <p>Thời gian bắt đầu : {convertDateFull(meetingL?.startTime)}</p>
               ....
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Hủy
               </Button>
               <Button variant="danger" onClick={onSubmit}>
                  Xóa
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
}
export default React.memo(TableMeeting);
