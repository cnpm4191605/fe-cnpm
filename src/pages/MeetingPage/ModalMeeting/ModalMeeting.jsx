import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./ModalMeeting.module.scss";
import { createMeeting, toggleModal } from "redux/meeting/action";

import { typeModal as modalType, genderType } from "utils/constants.js";
import { mapDateInput } from "utils/common.js";
const cx = classNames.bind(style);
function ModalResident() {
   const { typeModal, openModal, meeting } = useSelector(
      (state) => state.meeting
   );
   const dispatch = useDispatch();
   const [meetingInfo, setMeetingInfo] = useState({});
   const [errors, setErrors] = useState({});
   const [startTimeInput, setStartTime] = useState({});
   // console.log("check meetingInfo:", meetingInfo);

   useEffect(() => {
      if (typeModal === modalType.CREATE) {
         return;
      }
      if (typeModal === modalType.UPDATE) {
         console.log("check meeting:", meeting);
         if (JSON.stringify(meeting) !== "{}") {
            setMeetingInfo({ ...meeting });
         } else {
            dispatch(
               toggleModal({
                  typeModal: modalType.CREATE,
                  openModal: false,
               })
            );
         }
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [meeting]);
   useEffect(() => {
      let { hour, minute, date } = startTimeInput;
      let _hour = hour > 9 ? hour : `0${hour}`;
      let _minute = minute > 9 ? minute : `0${minute}`;
      setMeetingInfo({
         ...meetingInfo,
         startTime: `${date}T${_hour}:${_minute}`,
      });
   }, [startTimeInput]);
   const onChangeInput = (e, field) => {
      setMeetingInfo({
         ...meetingInfo,
         [field]: e.target.value,
      });
      // Check and see if errors exist, and remove them from the error object:
      if (!!errors[field])
         setErrors({
            ...errors,
            [field]: null,
         });
   };
   const onChangeInputTime = (e, field) => {
      setStartTime({
         ...startTimeInput,
         [field]: e.target.value,
      });

      // Check and see if errors exist, and remove them from the error object:
      if (!!errors[field])
         setErrors({
            ...errors,
            [field]: null,
         });
   };
   const findFormErrors = () => {
      const { title, location } = meetingInfo;
      const { date, hour, minute } = startTimeInput;
      const newErrors = {};
      // title errors
      if (!title || title === "")
         newErrors.title = "Tên cuộc họp không để trống";
      // location errors
      if (!location || location === "")
         newErrors.location = "Địa điểm không để trống";
      // startTimeInput errors
      if (!hour || hour > 23 || hour < 0)
         newErrors.hour = "Giờ trong khoảng từ 0-23h";
      if (!minute || minute > 59 || minute < 0)
         newErrors.minute = "Phút trong khoảng từ 0-59m";
      if (!date || new Date(date).getTime() < new Date().getTime())
         newErrors.date = "Ngày chọn phải lớn hơn hoặc bằng ngày hiện tại";
      return newErrors;
   };
   const onClose = () => {
      setMeetingInfo({});
      dispatch(
         toggleModal({
            typeModal: modalType.CREATE,
            openModal: false,
         })
      );
   };
   const onSubmit = () => {
      const newErrors = findFormErrors();
      // Conditional logic:
      if (Object.keys(newErrors).length > 0) {
         // We got errors!
         setErrors(newErrors);
         return;
      }
      if (typeModal === modalType.CREATE) {
         dispatch(createMeeting(meetingInfo));
      }
   };
   return (
      <Modal
         show={openModal}
         onHide={onClose}
         backdrop="static"
         size="lg"
         centered
      >
         <Modal.Header closeButton onHide={onClose}>
            <Modal.Title>
               {typeModal === modalType.UPDATE
                  ? "Cập nhật cuộc họp"
                  : "Thêm mới cuộc họp"}
            </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <Form>
               <Row>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="title">
                        <Form.Label>Chủ đề cuộc họp</Form.Label>
                        <Form.Control
                           required
                           type="text"
                           placeholder="Chủ đề cuộc họp"
                           value={meetingInfo?.title ?? ""}
                           onChange={(e) => onChangeInput(e, "title")}
                           isInvalid={!!errors.title}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.title}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="location">
                        <Form.Label>Địa điểm</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Địa điểm"
                           value={meetingInfo?.location ?? ""}
                           onChange={(e) => onChangeInput(e, "location")}
                           isInvalid={!!errors.location}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.location}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="namSinh">
                        <Form.Label>Thời gian bắt đầu</Form.Label>
                        <Row>
                           <Col>
                              <Form.Control
                                 type="number"
                                 placeholder="Giờ"
                                 min={0}
                                 max={23}
                                 value={startTimeInput?.hour ?? ""}
                                 onChange={(e) => onChangeInputTime(e, "hour")}
                                 isInvalid={!!errors.hour}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.hour}
                              </Form.Control.Feedback>
                           </Col>
                           <Col>
                              <Form.Control
                                 type="number"
                                 placeholder="Phút"
                                 min={0}
                                 max={59}
                                 value={startTimeInput?.minute ?? ""}
                                 onChange={(e) =>
                                    onChangeInputTime(e, "minute")
                                 }
                                 isInvalid={!!errors.minute}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.minute}
                              </Form.Control.Feedback>
                           </Col>
                           <Col>
                              <Form.Control
                                 type="date"
                                 value={startTimeInput?.date ?? ""}
                                 onChange={(e) => onChangeInputTime(e, "date")}
                                 isInvalid={!!errors.date}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.date}
                              </Form.Control.Feedback>
                           </Col>
                        </Row>
                     </Form.Group>
                  </Col>

                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="notes">
                        <Form.Label>Ghi chú</Form.Label>
                        <Form.Control
                           as="textarea"
                           placeholder="Ghi chú"
                           value={meetingInfo?.notes ?? ""}
                           onChange={(e) => onChangeInput(e, "notes")}
                        />
                     </Form.Group>
                  </Col>

                  <Col lg={6} sm={12}></Col>
               </Row>
            </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
               Đóng
            </Button>
            <Button variant="primary" onClick={onSubmit}>
               {typeModal === modalType.UPDATE ? "Cập nhật" : "Thêm mới"}
            </Button>
         </Modal.Footer>
      </Modal>
   );
}
export default React.memo(ModalResident);
