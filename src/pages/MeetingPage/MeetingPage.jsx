import { useSelector, useDispatch } from "react-redux";
import classNames from "classnames/bind";

import styles from "./MeetingPage.module.scss";
import { Button, Container, Col, Form, Row } from "react-bootstrap";
import { useCallback, useEffect, useState } from "react";
import { setArgPage, toggleModal } from "redux/household/action";
import { declarationType, genderType, typeModal } from "utils/constants";
// import * as householdService from "services/householdService";
// import { fieldHousehold, colHousehold } from "utils/constants";
import BasePaging from "components/Paging/BasePaging";
import { useDebounce } from "hook";
import ModalResident from "./ModalMeeting/ModalMeeting";
import TableResident from "./TableMeeting/TableMeeting";
import { toggleDeclaration } from "redux/resident/action";
let cx = classNames.bind(styles);
function ResidentPage() {
   const { openModal } = useSelector((state) => state.meeting);

   const dispatch = useDispatch();
   // useEffect(() => {
   //    dispatch(getHouseholdList());
   //    // eslint-disable-next-line react-hooks/exhaustive-deps
   // }, [limit, offset, keyword]);
   const handleOpenModal = () => {
      dispatch(
         toggleModal({
            typeModal: typeModal.CREATE,
            openModal: true,
         })
      );
   };
   return (
      <>
         <div className={cx("wrapper")}>
            <Container fluid>
               <h3 className={cx("title")}>Danh sách cuộc họp</h3>
               <div className={cx("content")}>
                  <Button
                     variant="primary"
                     onClick={() => handleOpenModal()}
                     className={cx("mb-3")}
                  >
                     Thêm mới cuộc hộp
                  </Button>
                  {/* <Row className={cx("toolbars")}>
                     <Col sm={12} lg={4} md={6}></Col>
                     <Col sm={12} lg={8} md={6}>
                        <Row className={cx("search")}>
                        
                        </Row>
                     </Col>
                  </Row> */}
                  <TableResident />
               </div>
            </Container>
         </div>
         {openModal && <ModalResident />}
      </>
   );
}
export default ResidentPage;
