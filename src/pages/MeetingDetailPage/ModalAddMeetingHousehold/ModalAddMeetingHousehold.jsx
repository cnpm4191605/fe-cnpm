import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, ListGroup, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./ModalAddMeetingHousehold.module.scss";
import {
   createResident,
   toggleModal,
   updateResidentById,
   createResidentInHousehold,
} from "redux/resident/action";

import { typeModal as modalType, genderType } from "utils/constants.js";
import { mapDateInput } from "utils/common.js";
import { useParams } from "react-router-dom";
import { useDebounce } from "hook";
import * as householdService from "services/householdService.js";
import { createMeetingHousehold } from "services/meetingService";
import { getHouseholdList } from "redux/household/action";
const cx = classNames.bind(style);
function ModalAddMeetingHousehold({ openModalAdd, handleCloseAddModal }) {
   const { typeModal, openModal, resident } = useSelector(
      (state) => state.resident
   );
   const { meetingId } = useParams();
   const dispatch = useDispatch();
   const [searchHouseholdModal, setSearchHouseholdModal] = useState("");
   const debounceValueModal = useDebounce(searchHouseholdModal, 500);
   const [showDropdown, setShowDropdown] = useState(false);
   const [households, setHouseholds] = useState([]);
   const [household, setHousehold] = useState({});
   console.log(household);
   useEffect(() => {
      if (!debounceValueModal || debounceValueModal === "") {
         setShowDropdown(false);

         return;
      }
      const fetchHouseholdsList = async () => {
         try {
            let res = await householdService.getHouseholdList(
               20,
               1,
               debounceValueModal
            );
            setHouseholds(res.data.items);
         } catch (error) {
            console.log(error);
            setHouseholds([]);
            toast.error("Lấy danh sách hộ khẩu thất bại");
         }
      };
      fetchHouseholdsList();
      setShowDropdown(true);

      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [debounceValueModal]);

   const onClose = () => {
      handleCloseAddModal();
   };
   const handleSelectedHousehold = (item) => {
      setShowDropdown(false);
      setHousehold({ ...item });
   };
   const onSubmit = async () => {
      /// validate
      if (JSON.stringify(household) === "{}") {
         toast.error("Chưa chọn hộ khẩu");
         return;
      }
      // check exist
      // try {
      //    handleCloseAddModal();
      //    await dispatch(getHouseholdList(meetingId));
      //    toast.success("Thêm hộ khẩu tham dự thành công");
      // } catch (error) {
      //    console.log(error);
      //    toast.error("Thêm hộ khẩu tham dự thất bại");
      // }
      try {
         await createMeetingHousehold({
            householdId: household.id,
            meetingId: meetingId,
         });
         handleCloseAddModal();
         await dispatch(getHouseholdList(meetingId));
         toast.success("Thêm hộ khẩu tham dự thành công");
      } catch (error) {
         console.log(error);
         toast.error("Thêm hộ khẩu tham dự thất bại");
      }
   };
   return (
      <Modal
         show={openModalAdd}
         onHide={() => onClose}
         backdrop="static"
         size="lg"
         centered
      >
         <Modal.Header closeButton onHide={onClose}>
            <Modal.Title>Thêm hộ khẩu tham dự</Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <Form>
               <Row>
                  <Col sm={12}>
                     <p>
                        {" "}
                        <b>Mã hộ khẩu:&nbsp;&nbsp;</b>
                        {`${household?.maHoKhau ?? ""}`}
                     </p>
                  </Col>
                  <Col sm={12}>
                     <p>
                        <b>Chủ hộ:&nbsp;&nbsp;</b>
                        {`${household?.householder?.hoTen ?? ""}`}
                     </p>
                  </Col>
                  <Col sm={12}>
                     <p>
                        <b>Địa chỉ:&nbsp;&nbsp;</b>
                        {`${household?.diaChi ?? ""}`}
                     </p>
                  </Col>
                  <Col lg={8} sm={8}>
                     <Form.Group className="mb-3" controlId="hoTen">
                        <Form.Label>Tìm kiếm</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Mã hộ khẩu / Mã khu vực / địa chỉ"
                           autoComplete="off"
                           value={searchHouseholdModal}
                           onChange={(e) =>
                              setSearchHouseholdModal(e.target.value)
                           }
                        />
                     </Form.Group>
                     {showDropdown && (
                        <ListGroup as="ul" className={cx("household-list")}>
                           {households.map((item) => (
                              <ListGroup.Item
                                 action
                                 variant="light"
                                 // as="li"
                                 key={item.id}
                                 onClick={() => handleSelectedHousehold(item)}
                                 className={cx("household-item")}
                              >
                                 {`Mã hộ khẩu: ${
                                    item?.maHoKhau ?? ""
                                 }, Chủ hộ: ${item?.householder?.hoTen ?? ""}`}
                              </ListGroup.Item>
                           ))}
                        </ListGroup>
                     )}
                  </Col>

                  <Col lg={6} sm={12}></Col>
               </Row>
            </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
               Đóng
            </Button>
            <Button variant="primary" onClick={onSubmit}>
               Thêm
            </Button>
         </Modal.Footer>
      </Modal>
   );
}
export default React.memo(ModalAddMeetingHousehold);
