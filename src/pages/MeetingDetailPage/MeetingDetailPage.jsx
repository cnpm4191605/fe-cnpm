import { useSelector, useDispatch } from "react-redux";
import classNames from "classnames/bind";
import { Button, Container } from "react-bootstrap";
import styles from "./MeetingDetail.module.scss";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMeetingById } from "redux/meeting/action";
import { setArgPage } from "redux/household/action";
import * as householdService from "services/householdService";
import { typeModal } from "utils/constants";
import BasePaging from "components/Paging/BasePaging";
import { useDebounce } from "hook";
import TableHousehold from "./TableHousehold/TableHousehold";
import ModalAddMeetingHousehold from "./ModalAddMeetingHousehold/ModalAddMeetingHousehold";
import { toggleModal } from "redux/resident/action";
import { convertDateFull } from "utils/common";
let cx = classNames.bind(styles);
function MeetingDetailPage() {
   const { meeting } = useSelector((state) => state.meeting);
   const { limit, offset, keyword, totalRecords } = useSelector(
      (state) => state.household
   );
   const { meetingId } = useParams();
   const [searchValue, setSearchValue] = useState("");
   const [openModalAdd, setOpenModalAdd] = useState(false);

   const debounceValue = useDebounce(searchValue, 500);

   const dispatch = useDispatch();
   useEffect(() => {
      dispatch(getMeetingById(Number(meetingId)));
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, []);
   useEffect(() => {
      dispatch(
         setArgPage({
            limit,
            offset: 1,
            keyword: debounceValue,
         })
      );
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [debounceValue]);
   const handleOpenAddModal = () => {
      setOpenModalAdd(true);
   };
   const handleCloseAddModal = () => {
      setOpenModalAdd(false);
   };
   const onChangePage = useCallback(
      (newOffset) => {
         dispatch(
            setArgPage({
               limit,
               offset: newOffset,
               keyword,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword]
   );

   const onChangeLimit = useCallback(
      (mewLimit) => {
         dispatch(
            setArgPage({
               limit: mewLimit,
               offset: 1,
               keyword,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword]
   );
   return (
      <>
         <div className={cx("wrapper")}>
            <Container>
               <h3 className={cx("title")}>Thông tin cuộc họp</h3>
               <div className={cx("content")}>
                  <div className={cx("meeting-info", "mb-2")}>
                     <p>
                        {" "}
                        <b>Tên cuộc họp:</b>
                        {meeting?.title}{" "}
                     </p>
                     <p>
                        <b>Địa điểm:</b> {meeting?.location}{" "}
                     </p>
                     <p>
                        <b>Thời gian bắt đầu:</b>{" "}
                        {convertDateFull(meeting?.startTime)}
                     </p>
                  </div>

                  <div className={cx("member-meeting")}>
                     <div className={cx("toolbars")}>
                        <h4 className="text-center mb-2">
                           Danh sách hộ tham dự
                        </h4>
                        <div className={cx("search")}>
                           <Button
                              variant="primary"
                              onClick={() => handleOpenAddModal()}
                           >
                              Thêm hộ khẩu tham dự
                           </Button>
                           <input
                              type="text"
                              className={cx("search-input")}
                              value={searchValue}
                              onChange={(e) =>
                                 setSearchValue(
                                    e.target.value[0] === " "
                                       ? ""
                                       : e.target.value
                                 )
                              }
                           />
                        </div>
                     </div>
                     <TableHousehold />
                     <BasePaging
                        limit={Number(limit)}
                        totalRecords={Number(totalRecords)}
                        offset={Number(offset)}
                        onChangePage={onChangePage}
                        onChangeLimit={onChangeLimit}
                     />
                  </div>
               </div>
            </Container>
         </div>

         {openModalAdd && (
            <ModalAddMeetingHousehold
               openModalAdd={openModalAdd}
               handleCloseAddModal={handleCloseAddModal}
            />
         )}
      </>
   );
}
export default MeetingDetailPage;
