import classNames from "classnames/bind";
import style from "./TableHousehold.module.scss";
import { Button, Modal, Table, Form } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { convertDate } from "utils/common";
import { path } from "utils/constants";
import { getHouseholdList } from "redux/household/action";
const cx = classNames.bind(style);

function TableHousehold() {
   const { households, limit, offset, keyword } = useSelector(
      (state) => state.household
   );
   const { meetingId } = useParams();
   const dispatch = useDispatch();

   const [showPopup, setShowPopup] = useState(false);
   const [householdR, setHousehold] = useState({});

   // console.log("households:", householdR);
   useEffect(() => {
      dispatch(getHouseholdList(meetingId));
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [meetingId, limit, offset, keyword]);
   const onRemoveItem = (item) => {
      setShowPopup(true);
      setHousehold({ ...item });
   };
   const onClose = () => {
      setHousehold({});
      setShowPopup(false);
   };
   const onSubmit = () => {
      // if (JSON.stringify(householdR) === "{}") {
      // } else {
      //    dispatch(removeResident(householdR.id));
      // }
      // setShowPopup(false);
   };
   console.log("re-render table householdR");
   return (
      <>
         <div className={cx("table-container")}>
            <Table striped bordered hover>
               <thead className={cx("table-header")}>
                  <tr>
                     <th>STT</th>
                     <th>Mã hộ khẩu</th>
                     <th>Mã khu vực</th>
                     <th>Chủ hộ</th>
                     <th>Năm sinh</th>
                     <th>Giới tính</th>
                     <th>Số CMT</th>
                     <th>Địa chỉ</th>
                     <th>Chức năng</th>
                  </tr>
               </thead>
               <tbody>
                  {households &&
                     households.length > 0 &&
                     households.map((item, index) => (
                        <tr key={item.id}>
                           <td>{index + 1}</td>
                           <td>
                              <Link to={`${path.HOUSEHOLD}/${item.id}`}>
                                 <b>{item?.maHoKhau}</b>
                              </Link>
                           </td>
                           <td>{item?.maKhuVuc}</td>
                           <td>{item?.householder?.hoTen}</td>
                           <td>
                              {convertDate(
                                 item?.householder?.namSinh ?? "invalid date"
                              )}
                           </td>
                           <td>{item?.householder?.gioiTinh}</td>
                           <td>{item?.householder?.soCMT}</td>
                           <td>{item?.diaChi}</td>
                           <td>
                              <Button
                                 variant="danger"
                                 className={cx("me-2")}
                                 onClick={() => onRemoveItem(item, index)}
                              >
                                 Xóa
                              </Button>
                           </td>
                        </tr>
                     ))}
               </tbody>
            </Table>
         </div>
         <Modal show={showPopup} onHide={onClose} size="md" centered>
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>Xóa nhân khẩu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               {/* <p>Ma ho hau :{householdR?.maHoKhau}</p> */}
               <p>Tên nhân khẩu : {householdR.hoTen}</p>
               ....
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Hủy
               </Button>
               <Button variant="danger" onClick={onSubmit}>
                  Xóa
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
}
export default React.memo(TableHousehold);
