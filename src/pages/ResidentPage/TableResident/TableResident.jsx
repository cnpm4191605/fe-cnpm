import classNames from "classnames/bind";
import style from "./TableResident.module.scss";
import { Button, Modal, Table, Form } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { convertDate } from "utils/common";
import { typeModal } from "utils/constants";
import {
   toggleModal,
   getResidentList,
   removeResident,
   getResidentById,
   setResidentIdSelected,
} from "redux/resident/action";
const cx = classNames.bind(style);

function TableResident() {
   const {
      residents,
      limit,
      offset,
      keyword,
      minAge,
      maxAge,
      gender,
      status,
      residentIdSelected,
   } = useSelector((state) => state.resident);
   const dispatch = useDispatch();

   const [showPopup, setShowPopup] = useState(false);
   const [residentR, setResident] = useState({});
   useEffect(() => {
      dispatch(getResidentList());
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [limit, offset, keyword, minAge, maxAge, gender, status]);
   const onRemoveItem = (item) => {
      setShowPopup(true);
      setResident({ ...item });
   };
   const onClose = () => {
      setResident({});
      setShowPopup(false);
   };
   const onSubmit = () => {
      if (JSON.stringify(residentR) === "{}") {
      } else {
         dispatch(removeResident(residentR.id));
      }
      setShowPopup(false);
      dispatch(setResidentIdSelected(-1));
   };
   const handleOpenModalUpdate = (residentId) => {
      dispatch(
         toggleModal({
            typeModal: typeModal.UPDATE,
            openModal: true,
         })
      );
      dispatch(getResidentById(residentId));
   };
   const onChangeResidentIdSelect = (residentId) => {
      dispatch(
         setResidentIdSelected(
            residentId === residentIdSelected ? -1 : residentId
         )
      );
   };
   return (
      <>
         <div className={cx("table-container")}>
            <Table striped bordered hover>
               <thead className={cx("table-header")}>
                  <tr>
                     <th>STT</th>
                     <th>Lựa chọn</th>
                     <th>Mã nhân khẩu</th>
                     <th>Họ tên</th>
                     <th>Biệt danh</th>
                     <th>Năm sinh</th>
                     <th>Giới tính</th>
                     <th>Số CMT</th>
                     <th>Dân tộc</th>
                     <th>Địa chỉ hiện nay</th>
                     <th>Nghề nghiệp</th>
                     <th>Nơi làm việc</th>
                     <th className={cx("tb-head-action")}>Chức năng</th>
                  </tr>
               </thead>
               <tbody>
                  {residents &&
                     residents.length > 0 &&
                     residents.map((item, index) => (
                        <tr
                           key={item.id}
                           className={cx(
                              residentIdSelected === item.id
                                 ? "row-selected"
                                 : ""
                           )}
                        >
                           <td>{index + 1}</td>
                           <td>
                              {" "}
                              <Form.Check
                                 // className="my-3"
                                 type="checkbox"
                                 // label="PayPal or Credit Card"
                                 // id="PayPal"
                                 // name="paymentMethod"
                                 // value={item.id}
                                 checked={residentIdSelected === item.id}
                                 onChange={() =>
                                    onChangeResidentIdSelect(item.id)
                                 }
                              />
                           </td>
                           <td>{item?.maNhanKhau}</td>
                           <td>{item?.hoTen}</td>
                           <td>{item?.bietDanh}</td>
                           <td>
                              {convertDate(item?.namSinh ?? "invalid date")}
                           </td>
                           <td>{item?.gioiTinh}</td>
                           <td>{item?.soCMT}</td>
                           <td>{item?.danToc}</td>
                           <td>{item?.diaChiHienNay}</td>
                           <td>{item?.ngheNghiep}</td>
                           <td>{item?.noiLamViec}</td>
                           <td>
                              <Button
                                 variant="danger"
                                 className={cx("me-2")}
                                 onClick={() => onRemoveItem(item, index)}
                              >
                                 Xóa
                              </Button>
                              <Button
                                 variant="info"
                                 onClick={() =>
                                    handleOpenModalUpdate(item?.id ?? -1)
                                 }
                              >
                                 Sửa
                              </Button>
                           </td>
                        </tr>
                     ))}
               </tbody>
            </Table>
         </div>
         <Modal show={showPopup} onHide={onClose} size="md" centered>
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>Xóa nhân khẩu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               {/* <p>Ma ho hau :{residentR?.maHoKhau}</p> */}
               <p>Tên nhân khẩu : {residentR.hoTen}</p>
               ....
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Hủy
               </Button>
               <Button variant="danger" onClick={onSubmit}>
                  Xóa
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
}
export default React.memo(TableResident);
