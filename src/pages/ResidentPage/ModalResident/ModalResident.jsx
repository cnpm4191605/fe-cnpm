import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./ModalResident.module.scss";
import {
   createResident,
   toggleModal,
   updateResidentById,
} from "redux/resident/action";

import { typeModal as modalType, genderType } from "utils/constants.js";
import { mapDateInput } from "utils/common.js";
import { useParams } from "react-router-dom";
const cx = classNames.bind(style);
function ModalResident() {
   const { typeModal, openModal, resident } = useSelector(
      (state) => state.resident
   );
   const dispatch = useDispatch();
   const [showMoreInfo, setShowMoreInfo] = useState(false);
   const [residentInfo, setResidentInfo] = useState({});
   const [errors, setErrors] = useState({});
   // console.log("check residentInfo:", residentInfo);

   useEffect(() => {
      if (typeModal === modalType.CREATE) {
         return;
      }
      if (typeModal === modalType.UPDATE) {
         // console.log("check resident:", resident);
         if (JSON.stringify(resident) !== "{}") {
            setResidentInfo({ ...resident });
         } else {
            dispatch(
               toggleModal({
                  typeModal: modalType.CREATE,
                  openModal: false,
               })
            );
         }
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [resident]);

   const onChangeInput = (e, field) => {
      setResidentInfo({
         ...residentInfo,
         [field]: e.target.value,
      });
      // Check and see if errors exist, and remove them from the error object:
      if (!!errors[field])
         setErrors({
            ...errors,
            [field]: null,
         });
   };
   const onClose = () => {
      setResidentInfo({});
      setErrors({});
      dispatch(
         toggleModal({
            typeModal: modalType.CREATE,
            openModal: false,
         })
      );
      setShowMoreInfo(false);
   };
   const findFormErrors = () => {
      const { hoTen, namSinh, gioiTinh, ngayCap, ngayChuyenDen, ngayChuyenDi } =
         residentInfo;
      const newErrors = {};
      if (!hoTen || hoTen === "")
         newErrors.hoTen = "Họ tên không được để trống";
      if (!gioiTinh || gioiTinh === "")
         newErrors.gioiTinh = "Giới tính không được để trống";
      if (!namSinh) {
         newErrors.namSinh = "Ngày sinh không được để trống";
      } else if (new Date(namSinh).getTime() > new Date().getTime())
         newErrors.namSinh = "Ngày sinh phải bé hơn hoặc bằng ngày hiện tại";
      if (ngayCap && new Date(ngayCap).getTime() > new Date().getTime())
         newErrors.ngayCap = "Ngày cấp phải bé hơn hoặc bằng ngày hiện tại";
      if (
         ngayChuyenDen &&
         new Date(ngayChuyenDen).getTime() > new Date().getTime()
      )
         newErrors.ngayChuyenDen =
            "Ngày chuyển đến phải bé hơn hoặc bằng ngày hiện tại";
      if (
         ngayChuyenDi &&
         new Date(ngayChuyenDi).getTime() > new Date().getTime()
      )
         newErrors.ngayChuyenDi =
            "Ngày chuyển đi phải bé hơn hoặc bằng ngày hiện tại";

      return newErrors;
   };
   const onSubmit = () => {
      const newErrors = findFormErrors();
      // Conditional logic:
      if (Object.keys(newErrors).length > 0) {
         // We got errors!
         setErrors(newErrors);
         return;
      }
      if (typeModal === modalType.CREATE) {
         dispatch(createResident(residentInfo));
      }
      if (typeModal === modalType.UPDATE) {
         dispatch(
            updateResidentById(residentInfo.id, {
               maNhanKhau: residentInfo.maNhanKhau,
               hoTen: residentInfo.hoTen,
               bietDanh: residentInfo.bietDanh,
               namSinh: residentInfo.namSinh,
               gioiTinh: residentInfo.gioiTinh,
               soCMT: residentInfo.soCMT,
               ngayCap: residentInfo.ngayCap,
               noiCap: residentInfo.noiCap,
               noiSinh: residentInfo.noiSinh,
               nguyenQuan: residentInfo.nguyenQuan,
               danToc: residentInfo.danToc,
               tonGiao: residentInfo.tonGiao,
               quocTich: residentInfo.quocTich,
               soHoChieu: residentInfo.soHoChieu,
               noiThuongTru: residentInfo.noiThuongTru,
               diaChiHienNay: residentInfo.diaChiHienNay,
               trinhDoHocVan: residentInfo.trinhDoHocVan,
               trinhDoChuyenMon: residentInfo.trinhDoChuyenMon,
               bietTiengDanToc: residentInfo.bietTiengDanToc,
               trinhDoNgoaiNgu: residentInfo.trinhDoNgoaiNgu,
               ngheNghiep: residentInfo.ngheNghiep,
               noiLamViec: residentInfo.noiLamViec,
               tienAn: residentInfo.tienAn,
               ngayChuyenDen: residentInfo.ngayChuyenDen,
               lyDoChuyenDen: residentInfo.lyDoChuyenDen,
               ngayChuyenDi: residentInfo.ngayChuyenDi,
               lyDoChuyenDi: residentInfo.lyDoChuyenDi,
               diaChiMoi: residentInfo.diaChiMoi,
               ghiChu: residentInfo.ghiChu,
            })
         );
      }
   };
   return (
      <Modal
         show={openModal}
         onHide={onClose}
         backdrop="static"
         size="lg"
         centered
      >
         <Modal.Header closeButton onHide={onClose}>
            <Modal.Title>
               {typeModal === modalType.UPDATE
                  ? "Cập nhật nhân khẩu"
                  : "Thêm mới nhân khẩu"}
            </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <Form>
               <Row>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="hoTen">
                        <Form.Label>
                           Họ và tên<span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Họ và tên"
                           value={residentInfo?.hoTen ?? ""}
                           onChange={(e) => onChangeInput(e, "hoTen")}
                           isInvalid={!!errors.hoTen}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.hoTen}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="bietDanh">
                        <Form.Label>Biệt danh</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Biệt danh"
                           value={residentInfo?.bietDanh ?? ""}
                           onChange={(e) => onChangeInput(e, "bietDanh")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="namSinh">
                        <Form.Label>
                           Năm sinh<span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="date"
                           placeholder="năm sinh"
                           value={mapDateInput(residentInfo?.namSinh ?? "")}
                           onChange={(e) => onChangeInput(e, "namSinh")}
                           isInvalid={!!errors.namSinh}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.namSinh}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="gioiTinh">
                        <Form.Label>Giới tính</Form.Label>
                        <Form.Control
                           as="select"
                           type="select"
                           size="md"
                           value={residentInfo?.gioiTinh ?? ""}
                           onChange={(e) => onChangeInput(e, "gioiTinh")}
                           isInvalid={!!errors.gioiTinh}
                        >
                           <option value=""></option>
                           <option value={genderType.MALE}>
                              {genderType.MALE}
                           </option>
                           <option value={genderType.FEMALE}>
                              {genderType.FEMALE}
                           </option>
                        </Form.Control>
                        <Form.Control.Feedback type="invalid">
                           {errors.gioiTinh}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="nguyenQuan">
                        <Form.Label>Nguyên quán</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Nguyên quán"
                           value={residentInfo?.nguyenQuan ?? ""}
                           onChange={(e) => onChangeInput(e, "nguyenQuan")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="tonGiao">
                        <Form.Label>Tôn giáo</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Tôn giáo"
                           value={residentInfo?.tonGiao ?? ""}
                           onChange={(e) => onChangeInput(e, "tonGiao")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="danToc">
                        <Form.Label>Dân tộc</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Dân tộc"
                           value={residentInfo?.danToc ?? ""}
                           onChange={(e) => onChangeInput(e, "danToc")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="quocTich">
                        <Form.Label>Quốc tịch</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Quốc tịch"
                           value={residentInfo?.quocTich ?? ""}
                           onChange={(e) => onChangeInput(e, "quocTich")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="soCMT">
                        <Form.Label>Số CMT/ CCCD</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Số CMT/ CCCD"
                           value={residentInfo?.soCMT ?? ""}
                           onChange={(e) => onChangeInput(e, "soCMT")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="soHoChieu">
                        <Form.Label>Số hộ chiếu</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Số hộ chiếu"
                           value={residentInfo?.soHoChieu ?? ""}
                           onChange={(e) => onChangeInput(e, "soHoChieu")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="noiThuongTru">
                        <Form.Label>Nơi thường chú</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Nơi thường chú"
                           value={residentInfo?.noiThuongTru ?? ""}
                           onChange={(e) => onChangeInput(e, "noiThuongTru")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="diaChiHienNay">
                        <Form.Label>Địa chỉ hiện nay</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Địa chỉ hiện nay"
                           value={residentInfo?.diaChiHienNay ?? ""}
                           onChange={(e) => onChangeInput(e, "diaChiHienNay")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="trinhDoHocVan">
                        <Form.Label>Trình độ học vấn</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Trình độ học vấn"
                           value={residentInfo?.trinhDoHocVan ?? ""}
                           onChange={(e) => onChangeInput(e, "trinhDoHocVan")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="trinhDoChuyenMon">
                        <Form.Label>Trình độ chuyên môn</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Trình độ chuyên môn"
                           value={residentInfo?.trinhDoChuyenMon ?? ""}
                           onChange={(e) =>
                              onChangeInput(e, "trinhDoChuyenMon")
                           }
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="trinhDoNgoaiNgu">
                        <Form.Label>Trình độ ngoại ngữ</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Trình độ ngoại ngữ"
                           value={residentInfo?.trinhDoNgoaiNgu ?? ""}
                           onChange={(e) => onChangeInput(e, "trinhDoNgoaiNgu")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="bietTiengDanToc">
                        <Form.Label>Biết tiếng dân tộc</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Biết tiếng dân tộc"
                           value={residentInfo?.bietTiengDanToc ?? ""}
                           onChange={(e) => onChangeInput(e, "bietTiengDanToc")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="ngheNghiep">
                        <Form.Label>Nghề nghiệp</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Nghề nghiệp"
                           value={residentInfo?.ngheNghiep ?? ""}
                           onChange={(e) => onChangeInput(e, "ngheNghiep")}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="noiLamViec">
                        <Form.Label>Nơi làm việc</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Nơi làm việc"
                           value={residentInfo?.noiLamViec ?? ""}
                           onChange={(e) => onChangeInput(e, "noiLamViec")}
                        />
                     </Form.Group>
                  </Col>
                  {showMoreInfo && (
                     <>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="ngayCap">
                              <Form.Label>Ngày cấp CMT/ CCCD</Form.Label>
                              <Form.Control
                                 type="date"
                                 placeholder="Ngày cấp CMT/ CCCD"
                                 value={residentInfo?.ngayCap ?? ""}
                                 onChange={(e) => onChangeInput(e, "ngayCap")}
                                 isInvalid={!!errors.ngayCap}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.ngayCap}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="noiCap">
                              <Form.Label>Nơi cấp CMT/ CCCD</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Nơi cấp CMT/ CCCD"
                                 value={residentInfo?.noiCap ?? ""}
                                 onChange={(e) => onChangeInput(e, "noiCap")}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="tienAn">
                              <Form.Label>Tiền án</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Tiền án"
                                 value={residentInfo?.tienAn ?? ""}
                                 onChange={(e) => onChangeInput(e, "tienAn")}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group
                              className="mb-3"
                              controlId="ngayChuyenDen"
                           >
                              <Form.Label>Ngày chuyển đến</Form.Label>
                              <Form.Control
                                 type="date"
                                 placeholder="Ngày chuyển đến"
                                 value={residentInfo?.ngayChuyenDen ?? ""}
                                 onChange={(e) =>
                                    onChangeInput(e, "ngayChuyenDen")
                                 }
                                 isInvalid={!!errors.ngayChuyenDen}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.ngayChuyenDen}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group
                              className="mb-3"
                              controlId="lyDoChuyenDen"
                           >
                              <Form.Label>Lý do chuyển đến</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Lý do chuyển đến"
                                 value={residentInfo?.lyDoChuyenDen ?? ""}
                                 onChange={(e) =>
                                    onChangeInput(e, "lyDoChuyenDen")
                                 }
                              />
                           </Form.Group>
                        </Col>
                        {typeModal === modalType.UPDATE && (
                           <>
                              <Col lg={6} sm={12}>
                                 <Form.Group
                                    className="mb-3"
                                    controlId="ngayChuyenDi"
                                 >
                                    <Form.Label>Ngày chuyển đi</Form.Label>
                                    <Form.Control
                                       type="date"
                                       placeholder="Ngày chuyển đi"
                                       value={residentInfo?.ngayChuyenDi ?? ""}
                                       onChange={(e) =>
                                          onChangeInput(e, "ngayChuyenDi")
                                       }
                                       isInvalid={!!errors.ngayChuyenDi}
                                    />
                                    <Form.Control.Feedback type="invalid">
                                       {errors.ngayChuyenDi}
                                    </Form.Control.Feedback>
                                 </Form.Group>
                              </Col>
                              <Col lg={6} sm={12}>
                                 <Form.Group
                                    className="mb-3"
                                    controlId="lyDoChuyenDi"
                                 >
                                    <Form.Label>Lý do chuyển đi</Form.Label>
                                    <Form.Control
                                       type="text"
                                       placeholder="Lý do chuyển đi"
                                       value={residentInfo?.lyDoChuyenDi ?? ""}
                                       onChange={(e) =>
                                          onChangeInput(e, "lyDoChuyenDi")
                                       }
                                    />
                                 </Form.Group>
                              </Col>
                              <Col lg={6} sm={12}>
                                 <Form.Group
                                    className="mb-3"
                                    controlId="diaChiMoi"
                                 >
                                    <Form.Label>Địa chỉ mới</Form.Label>
                                    <Form.Control
                                       type="text"
                                       placeholder="Địa chỉ mới"
                                       value={residentInfo?.diaChiMoi ?? ""}
                                       onChange={(e) =>
                                          onChangeInput(e, "diaChiMoi")
                                       }
                                    />
                                 </Form.Group>
                              </Col>
                           </>
                        )}
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="ghiChu">
                              <Form.Label>Ghi chú</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Ghi chú"
                                 value={residentInfo?.ghiChu ?? ""}
                                 onChange={(e) => onChangeInput(e, "ghiChu")}
                              />
                           </Form.Group>
                        </Col>
                     </>
                  )}
                  <Button
                     variant="secondary"
                     onClick={() => setShowMoreInfo((prevState) => !prevState)}
                  >
                     {showMoreInfo ? "Thu gọn" : "Khác..."}
                  </Button>

                  <Col lg={6} sm={12}></Col>
               </Row>
            </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
               Đóng
            </Button>
            <Button variant="primary" onClick={onSubmit}>
               {typeModal === modalType.UPDATE ? "Cập nhật" : "Thêm mới"}
            </Button>
         </Modal.Footer>
      </Modal>
   );
}
export default React.memo(ModalResident);
