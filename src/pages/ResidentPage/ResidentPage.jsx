import { useSelector, useDispatch } from "react-redux";
import classNames from "classnames/bind";

import styles from "./Resident.module.scss";
import {
   Button,
   Container,
   Col,
   Form,
   Row,
   DropdownButton,
   Dropdown,
} from "react-bootstrap";
import { useCallback, useEffect, useState } from "react";

import { declarationType, genderType, typeModal } from "utils/constants";
// import * as householdService from "services/householdService";
// import { fieldHousehold, colHousehold } from "utils/constants";
import BasePaging from "components/Paging/BasePaging";
import { useDebounce } from "hook";
import ModalResident from "./ModalResident/ModalResident";
import TableResident from "./TableResident/TableResident";
import {
   toggleDeclaration,
   toggleModal,
   setArgPage,
   getResidentById,
} from "redux/resident/action";
import FormDeclaration from "./FormDeclaration/FormDeclaration";
let cx = classNames.bind(styles);
function ResidentPage() {
   const {
      limit,
      offset,
      keyword,
      minAge,
      maxAge,
      gender,
      status,
      totalRecords,
      openModal,
      residentIdSelected,
   } = useSelector((state) => state.resident);
   const [searchKeywordValue, setSearchKeywordValue] = useState("");
   const [searchMinAgeValue, setSearchMinAgeValue] = useState(0);
   const [searchMaxAgeValue, setSearchMaxAgeValue] = useState(150);
   const [genderSelected, setGenderSelected] = useState("");
   const [statusSelected, setStatusSelected] = useState("");
   const debounceKeywordValue = useDebounce(searchKeywordValue, 500);
   const debounceMinAgeValue = useDebounce(searchMinAgeValue, 500);
   const debounceMaxAgeValue = useDebounce(searchMaxAgeValue, 500);
   const dispatch = useDispatch();
   // useEffect(() => {
   //    dispatch(getHouseholdList());
   //    // eslint-disable-next-line react-hooks/exhaustive-deps
   // }, [limit, offset, keyword]);

   useEffect(() => {
      dispatch(
         setArgPage({
            limit,
            offset: 1,
            keyword: debounceKeywordValue,
            minAge: debounceMinAgeValue ? debounceMinAgeValue : 0,
            maxAge: debounceMaxAgeValue ? debounceMaxAgeValue : 150,
            gender: genderSelected,
            status: statusSelected,
         })
      );
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [
      debounceKeywordValue,
      debounceMinAgeValue,
      debounceMaxAgeValue,
      genderSelected,
      statusSelected,
   ]);
   useEffect(() => {}, [residentIdSelected]);

   const onChangePage = useCallback(
      (newOffset) => {
         dispatch(
            setArgPage({
               limit,
               offset: newOffset,
               keyword,
               minAge,
               maxAge,
               gender,
               status,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword, minAge, maxAge, gender, status]
   );

   const onChangeLimit = useCallback(
      (mewLimit) => {
         dispatch(
            setArgPage({
               limit: mewLimit,
               offset: 1,
               keyword,
               minAge,
               maxAge,
               gender,
               status,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword, minAge, maxAge, gender, status]
   );

   const handleOpenModal = () => {
      dispatch(
         toggleModal({
            typeModal: typeModal.CREATE,
            openModal: true,
         })
      );
   };
   const openFormDeclaration = async (typeDeclaration) => {
      await dispatch(getResidentById(residentIdSelected));
      dispatch(
         toggleDeclaration({
            typeDeclaration: typeDeclaration,
            openDeclaration: true,
         })
      );
   };
   return (
      <>
         <div className={cx("wrapper")}>
            <Container fluid>
               <h3 className={cx("title")}>Danh sách nhân khẩu khu dân cư</h3>
               <div className={cx("content")}>
                  <Button variant="primary" onClick={() => handleOpenModal()}>
                     Thêm mới nhân khẩu
                  </Button>
                  <Row className={cx("toolbars")}>
                     <Col sm={12} lg={4} md={6}>
                        <DropdownButton
                           id="dropdown-basic-button"
                           title="Chức năng khác"
                           disabled={residentIdSelected === -1}
                        >
                           <Dropdown.Item
                              onClick={() =>
                                 openFormDeclaration(
                                    declarationType.TEMPORARY_RESIDENCE
                                 )
                              }
                           >
                              Tạm trú
                           </Dropdown.Item>
                           <Dropdown.Item
                              onClick={() =>
                                 openFormDeclaration(
                                    declarationType.TEMPORARY_ABSENT
                                 )
                              }
                           >
                              Tạm vắng
                           </Dropdown.Item>
                           <Dropdown.Item href="#">Khai tử</Dropdown.Item>
                        </DropdownButton>
                     </Col>
                     <Col sm={12} lg={8} md={6}>
                        <Row className={cx("search")}>
                           <Col
                              md={12}
                              lg={8}
                              className={cx("filter-age", "mb-3")}
                           >
                              <span>
                                 <b> Độ tuổi:&nbsp;&nbsp;Từ&nbsp;&nbsp; </b>
                              </span>
                              <Form.Control
                                 type="number"
                                 min="10"
                                 max="150"
                                 className={cx("minAge-input")}
                                 value={searchMinAgeValue}
                                 onChange={(e) =>
                                    setSearchMinAgeValue(e.target.value)
                                 }
                              />
                              <span>
                                 <b>&nbsp;&nbsp; đến&nbsp;&nbsp; </b>
                              </span>
                              <Form.Control
                                 type="number"
                                 min="10"
                                 max="150"
                                 className={cx("maxAge-input")}
                                 value={searchMaxAgeValue}
                                 onChange={(e) =>
                                    setSearchMaxAgeValue(e.target.value)
                                 }
                              />
                           </Col>
                           <Col
                              md={12}
                              lg={4}
                              className={cx("filter-gender", "mb-3")}
                           >
                              <span>
                                 <b>Giới tính:</b>
                              </span>
                              <Form.Select
                                 value={gender ?? ""}
                                 onChange={(e) =>
                                    setGenderSelected(e.target.value)
                                 }
                              >
                                 <option value="">Toàn bộ</option>
                                 <option value={genderType.MALE}>
                                    {genderType.MALE}
                                 </option>
                                 <option value={genderType.FEMALE}>
                                    {genderType.FEMALE}
                                 </option>
                              </Form.Select>
                           </Col>
                           <Col
                              md={12}
                              lg={6}
                              className={cx("filter-gender", "mb-3")}
                           >
                              <span>
                                 <b>Tình trạng:</b>
                              </span>
                              <Form.Select
                                 value={status ?? ""}
                                 onChange={(e) =>
                                    setStatusSelected(e.target.value)
                                 }
                              >
                                 <option value="">Toàn bộ</option>
                                 <option
                                    value={declarationType.TEMPORARY_ABSENT}
                                 >
                                    {"Tạm vắng"}
                                 </option>
                                 <option
                                    value={declarationType.TEMPORARY_RESIDENCE}
                                 >
                                    {"Tạm trú"}
                                 </option>
                              </Form.Select>
                           </Col>
                           <Col md={12} lg={6} className={cx("mb-3")}>
                              <Form.Control
                                 type="text"
                                 className={cx("search-input")}
                                 placeholder="Nhập từ khóa tìm kiếm"
                                 value={searchKeywordValue}
                                 onChange={(e) =>
                                    setSearchKeywordValue(
                                       e.target.value[0] === " "
                                          ? ""
                                          : e.target.value
                                    )
                                 }
                              />
                           </Col>
                        </Row>
                     </Col>
                  </Row>
                  <TableResident />
                  <BasePaging
                     limit={Number(limit)}
                     totalRecords={Number(totalRecords)}
                     offset={Number(offset)}
                     onChangePage={onChangePage}
                     onChangeLimit={onChangeLimit}
                  />
               </div>
            </Container>
         </div>
         <ModalResident />
         <FormDeclaration />
      </>
   );
}
export default ResidentPage;
