import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./FormDeclaration.module.scss";
import { getResidentById, toggleDeclaration } from "redux/resident/action";
import * as temporaryResidenceService from "services/temporaryResidenceService.js";
import * as temporaryAbsentService from "services/temporaryAbsentService.js";
import { declarationType } from "utils/constants.js";
import { convertDate } from "utils/common.js";
const cx = classNames.bind(style);
function FormDeclaration() {
   const { resident, typeDeclaration, openDeclaration } = useSelector(
      (state) => state.resident
   );
   const dispatch = useDispatch();
   const [declarationInfo, setDeclarationInfo] = useState({});
   const [showDeclaration, setShowDeclaration] = useState(false);
   const [errors, setErrors] = useState({});

   console.log("declarationInfo", declarationInfo);
   console.log(openDeclaration);
   useEffect(() => {
      if (openDeclaration) {
         // dispatch(getResidentById(residentIdSelected));
         if (
            [
               declarationType.TEMPORARY_RESIDENCE,
               declarationType.TEMPORARY_ABSENT,
            ].includes(typeDeclaration)
         ) {
            setDeclarationInfo({
               idNhanKhau: resident.id,
            });
         }
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [typeDeclaration, openDeclaration]);

   const onChangeInput = (e, field) => {
      setDeclarationInfo({
         ...declarationInfo,
         [field]: e.target.value,
      });
      // Check and see if errors exist, and remove them from the error object:
      if (!!errors[field])
         setErrors({
            ...errors,
            [field]: null,
         });
   };
   const onClose = () => {
      setDeclarationInfo({});
      dispatch(
         toggleDeclaration({
            typeDeclaration: "",
            openDeclaration: false,
         })
      );
   };
   const onCloseDeclarationDetail = () => {
      setShowDeclaration(false);
      dispatch(
         toggleDeclaration({
            typeDeclaration: "",
            openDeclaration: false,
         })
      );
   };
   const findFormErrors = () => {
      const newErrors = {};
      if (typeDeclaration === declarationType.TEMPORARY_RESIDENCE) {
         const { maGiayTamtru, soDienThoaiNguoiDangKy, tuNgay, denNgay, lyDo } =
            declarationInfo;
         // title errors
         if (!maGiayTamtru || maGiayTamtru === "")
            newErrors.maGiayTamtru = "Trường này không để trống";
         // location errors
         if (!soDienThoaiNguoiDangKy || soDienThoaiNguoiDangKy === "")
            newErrors.soDienThoaiNguoiDangKy = "Trường này không để trống";

         if (!tuNgay) newErrors.tuNgay = "Trường này không để trống";
         if (!denNgay) newErrors.denNgay = "Trường này không để trống";
         if (
            tuNgay &&
            denNgay &&
            new Date(tuNgay).getTime() > new Date(denNgay).getTime()
         ) {
            newErrors.denNgay = "Ngày kết thúc phải lơn hơn ngày bắt đầu";
         }
         if (!lyDo || lyDo === "") newErrors.lyDo = "Trường này không để trống";
      }
      if (typeDeclaration === declarationType.TEMPORARY_ABSENT) {
         const { maGiayTamVang, noiTamtru, tuNgay, denNgay, lyDo } =
            declarationInfo;
         // title errors
         if (!maGiayTamVang || maGiayTamVang === "")
            newErrors.maGiayTamVang = "Trường này không để trống";
         // location errors
         if (!noiTamtru || noiTamtru === "")
            newErrors.noiTamtru = "Trường này không để trống";

         if (!tuNgay) newErrors.tuNgay = "Trường này không để trống";
         if (!denNgay) newErrors.denNgay = "Trường này không để trống";
         if (
            tuNgay &&
            denNgay &&
            new Date(tuNgay).getTime() > new Date(denNgay).getTime()
         ) {
            newErrors.denNgay = "Ngày kết thúc phải lơn hơn ngày bắt đầu";
         }
         if (!lyDo || lyDo === "") newErrors.lyDo = "Trường này không để trống";
      }

      return newErrors;
   };
   const onSubmit = async () => {
      const newErrors = findFormErrors();
      // Conditional logic:
      if (Object.keys(newErrors).length > 0) {
         // We got errors!
         setErrors(newErrors);
         return;
      }
      if (typeDeclaration === declarationType.TEMPORARY_RESIDENCE) {
         try {
            await temporaryResidenceService.createTemporaryResidence(
               declarationInfo
            );
            toast.success("Khai báo tạm trú thành công");
            setShowDeclaration(true);
            dispatch(
               toggleDeclaration({
                  typeDeclaration,
                  openDeclaration: false,
               })
            );
         } catch (error) {
            console.log(error);
            toast.error("Khai báo tạm trú thất bại");
         }
      }
      if (typeDeclaration === declarationType.TEMPORARY_ABSENT) {
         try {
            await temporaryAbsentService.createTemporaryAbsent(declarationInfo);
            toast.success("Khai báo tạm vắng thành công");
            setShowDeclaration(true);
            dispatch(
               toggleDeclaration({
                  typeDeclaration,
                  openDeclaration: false,
               })
            );
         } catch (error) {
            console.log(error);
            toast.error("Khai báo tạm vắng thất bại");
         }
      }
   };
   return (
      <>
         <Modal
            show={openDeclaration}
            onHide={onClose}
            backdrop="static"
            size="lg"
            centered
         >
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>
                  {typeDeclaration === declarationType.TEMPORARY_RESIDENCE
                     ? "Khai báo tạm trú"
                     : typeDeclaration === declarationType.TEMPORARY_ABSENT
                     ? "Khai báo tạm vắng"
                     : typeDeclaration === declarationType.DECLARE_DEATH
                     ? "Khai tử"
                     : ".... Have some error"}
               </Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <Form>
                  {typeDeclaration === declarationType.TEMPORARY_RESIDENCE && (
                     <Row>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="hoTen">
                              <Form.Label>Họ và tên</Form.Label>
                              <Form.Control
                                 type="text"
                                 disabled
                                 placeholder="Họ và tên"
                                 value={resident?.hoTen ?? ""}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="soCMT">
                              <Form.Label>Số CMT/ CCCD</Form.Label>
                              <Form.Control
                                 type="text"
                                 disabled
                                 placeholder="Số CMT/ CCCD"
                                 value={resident?.soCMT ?? ""}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group
                              className="mb-3"
                              controlId="maGiayTamtru"
                           >
                              <Form.Label>Mã giấy tạm trú</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Mã giấy tạm trú"
                                 value={declarationInfo?.maGiayTamtru ?? ""}
                                 onChange={(e) =>
                                    onChangeInput(e, "maGiayTamtru")
                                 }
                                 isInvalid={!!errors.maGiayTamtru}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.maGiayTamtru}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group
                              className="mb-3"
                              controlId="soDienThoaiNguoiDangKy"
                           >
                              <Form.Label>
                                 Số điện thoại người đăng ký
                              </Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Số điện thoại người đăng ký"
                                 value={
                                    declarationInfo?.soDienThoaiNguoiDangKy ??
                                    ""
                                 }
                                 onChange={(e) =>
                                    onChangeInput(e, "soDienThoaiNguoiDangKy")
                                 }
                                 isInvalid={!!errors.soDienThoaiNguoiDangKy}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.soDienThoaiNguoiDangKy}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="tuNgay">
                              <Form.Label>Từ ngày</Form.Label>
                              <Form.Control
                                 type="date"
                                 value={declarationInfo?.tuNgay ?? ""}
                                 onChange={(e) => onChangeInput(e, "tuNgay")}
                                 isInvalid={!!errors.tuNgay}
                              />
                           </Form.Group>
                           <Form.Control.Feedback type="invalid">
                              {errors.tuNgay}
                           </Form.Control.Feedback>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="denNgay">
                              <Form.Label>Đến ngày</Form.Label>
                              <Form.Control
                                 type="date"
                                 value={declarationInfo?.denNgay ?? ""}
                                 onChange={(e) => onChangeInput(e, "denNgay")}
                                 isInvalid={!!errors.denNgay}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.denNgay}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={12} sm={12}>
                           <Form.Group className="mb-3" controlId="lyDo">
                              <Form.Label>Lý do</Form.Label>
                              <Form.Control
                                 as="textarea"
                                 rows={3}
                                 value={declarationInfo?.lyDo ?? ""}
                                 onChange={(e) => onChangeInput(e, "lyDo")}
                                 isInvalid={!!errors.lyDo}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.lyDo}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}></Col>
                     </Row>
                  )}
                  {typeDeclaration === declarationType.TEMPORARY_ABSENT && (
                     <Row>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="hoTen">
                              <Form.Label>Họ và tên</Form.Label>
                              <Form.Control
                                 type="text"
                                 disabled
                                 placeholder="Họ và tên"
                                 value={resident?.hoTen ?? ""}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="soCMT">
                              <Form.Label>Số CMT/ CCCD</Form.Label>
                              <Form.Control
                                 type="text"
                                 disabled
                                 placeholder="Số CMT/ CCCD"
                                 value={resident?.soCMT ?? ""}
                              />
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group
                              className="mb-3"
                              controlId="maGiayTamVang"
                           >
                              <Form.Label>Mã giấy tạm vắng</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Mã giấy tạm vắng"
                                 value={declarationInfo?.maGiayTamVang ?? ""}
                                 onChange={(e) =>
                                    onChangeInput(e, "maGiayTamVang")
                                 }
                                 isInvalid={!!errors.maGiayTamVang}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.maGiayTamVang}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="noiTamtru">
                              <Form.Label>Nơi tạm trú</Form.Label>
                              <Form.Control
                                 type="text"
                                 placeholder="Nơi tạm trú"
                                 value={declarationInfo?.noiTamtru ?? ""}
                                 onChange={(e) => onChangeInput(e, "noiTamtru")}
                                 isInvalid={!!errors.noiTamtru}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.noiTamtru}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="tuNgay">
                              <Form.Label>Từ ngày</Form.Label>
                              <Form.Control
                                 type="date"
                                 value={declarationInfo?.tuNgay ?? ""}
                                 onChange={(e) => onChangeInput(e, "tuNgay")}
                                 isInvalid={!!errors.tuNgay}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.tuNgay}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}>
                           <Form.Group className="mb-3" controlId="denNgay">
                              <Form.Label>Đến ngày</Form.Label>
                              <Form.Control
                                 type="date"
                                 value={declarationInfo?.denNgay ?? ""}
                                 onChange={(e) => onChangeInput(e, "denNgay")}
                                 isInvalid={!!errors.denNgay}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.denNgay}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={12} sm={12}>
                           <Form.Group className="mb-3" controlId="lyDo">
                              <Form.Label>Lý do</Form.Label>
                              <Form.Control
                                 as="textarea"
                                 rows={3}
                                 value={declarationInfo?.lyDo ?? ""}
                                 onChange={(e) => onChangeInput(e, "lyDo")}
                                 isInvalid={!!errors.lyDo}
                              />
                              <Form.Control.Feedback type="invalid">
                                 {errors.lydo}
                              </Form.Control.Feedback>
                           </Form.Group>
                        </Col>
                        <Col lg={6} sm={12}></Col>
                     </Row>
                  )}
               </Form>
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Đóng
               </Button>
               <Button variant="primary" onClick={onSubmit}>
                  Khai báo
               </Button>
            </Modal.Footer>
         </Modal>
         <Modal
            show={showDeclaration}
            onHide={onCloseDeclarationDetail}
            backdrop="static"
            size="md"
            centered
         >
            <Modal.Header closeButton onHide={onCloseDeclarationDetail}>
               {typeDeclaration === declarationType.TEMPORARY_RESIDENCE && (
                  <Modal.Title> Đơn khai báo tạm trú</Modal.Title>
               )}
               {typeDeclaration === declarationType.TEMPORARY_ABSENT && (
                  <Modal.Title> Đơn khai báo tạm vắng</Modal.Title>
               )}
            </Modal.Header>
            <Modal.Body>
               {typeDeclaration === declarationType.TEMPORARY_RESIDENCE && (
                  <>
                     <p>Mã giấy tạm trú : {declarationInfo?.maGiayTamtru}</p>
                     <p>Họ tên :{resident?.hoTen}</p>
                     <p>Số CMT/ CCCD : {resident?.soCMT}</p>
                     <p>
                        Số điện thoại đăng ký :{" "}
                        {declarationInfo?.soDienThoaiNguoiDangKy}
                     </p>
                     <p>
                        Từ ngày : {convertDate(declarationInfo?.tuNgay ?? "")}
                     </p>
                     <p>
                        Đến ngày : {convertDate(declarationInfo?.denNgay ?? "")}
                     </p>
                     <p>Lý do : {declarationInfo.lyDo}</p>
                  </>
               )}
               {typeDeclaration === declarationType.TEMPORARY_ABSENT && (
                  <>
                     <p>Mã giấy tạm vắng : {declarationInfo?.maGiayTamVang}</p>
                     <p>Họ tên :{resident?.hoTen}</p>
                     <p>Số CMT/ CCCD : {resident?.soCMT}</p>
                     <p>Nơi tạm trú : {declarationInfo?.noiTamtru}</p>
                     <p>
                        Từ ngày : {convertDate(declarationInfo?.tuNgay ?? "")}
                     </p>
                     <p>
                        Đến ngày : {convertDate(declarationInfo?.denNgay ?? "")}
                     </p>
                     <p>Lý do : {declarationInfo.lyDo}</p>
                  </>
               )}
            </Modal.Body>
         </Modal>
      </>
   );
}
export default React.memo(FormDeclaration);
