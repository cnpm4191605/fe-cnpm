import classNames from "classnames/bind";
import style from "./TableHouseholdDetail.module.scss";
import { Button, Modal, Table, Form } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { convertDate } from "utils/common";
import { typeModal } from "utils/constants";
import {
   toggleModal,
   getResidentList,
   getResidentListByHouseholdId,
   removeResident,
   getResidentById,
   getMemberHouseholdById,
} from "redux/resident/action";
import { deleteMemberHouseholdByResidentId } from "services/memberHouseholdService";
import { toast } from "react-toastify";
const cx = classNames.bind(style);

function TableHouseholdDetail({ household }) {
   const { residents } = useSelector((state) => state.resident);
   const { householdID } = useParams();
   const dispatch = useDispatch();

   const [showPopup, setShowPopup] = useState(false);
   const [residentR, setResident] = useState({});
   useEffect(() => {
      dispatch(getResidentListByHouseholdId(householdID));
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [householdID]);
   const onRemoveItem = (item) => {
      setShowPopup(true);
      setResident({ ...item });
   };
   const onClose = () => {
      setResident({});
      setShowPopup(false);
   };
   const onSubmit = async () => {
      if (JSON.stringify(residentR) === "{}") {
      } else {
         try {
            await deleteMemberHouseholdByResidentId(residentR.idNhanKhau);
            await dispatch(getResidentListByHouseholdId(householdID));
            toast.success("Xóa quan hệ thành công");
         } catch (error) {
            console.log(error);
            toast.error("Xóa quan hệ thất bại");
         }
      }
      setShowPopup(false);
   };
   const handleOpenModalUpdate = (id) => {
      dispatch(getMemberHouseholdById(id));
      dispatch(
         toggleModal({
            typeModal: typeModal.UPDATE,
            openModal: true,
         })
      );
   };
   return (
      <>
         <div className={cx("table-container")}>
            <Table striped bordered hover>
               <thead className={cx("table-header")}>
                  <tr>
                     <th>STT</th>
                     <th>Họ tên</th>
                     <th>Biệt danh</th>
                     <th>Quan hệ với chủ hộ</th>
                     <th>Năm sinh</th>
                     <th>Giới tính</th>
                     <th>Số CMT</th>
                     <th>Dân tộc</th>
                     <th>Địa chỉ hiện nay</th>
                     <th>Nghề nghiệp</th>
                     <th>Nơi làm việc</th>
                     <th className={cx("tb-head-action")}>Chức năng</th>
                  </tr>
               </thead>
               <tbody>
                  {residents &&
                     residents.length > 0 &&
                     residents.map((item, index) => (
                        <tr key={item.id}>
                           <td>{index + 1}</td>
                           <td>{item?.Resident?.hoTen}</td>
                           <td>{item?.Resident?.bietDanh}</td>
                           <td>{item?.quanHeVoiChuHo}</td>
                           <td>
                              {convertDate(item?.namSinh ?? "invalid date")}
                           </td>
                           <td>{item?.Resident?.gioiTinh}</td>
                           <td>{item?.Resident?.soCMT}</td>
                           <td>{item?.Resident?.danToc}</td>
                           <td>{item?.Resident?.diaChiHienNay}</td>
                           <td>{item?.Resident?.ngheNghiep}</td>
                           <td>{item?.Resident?.noiLamViec}</td>
                           <td>
                              {!(household?.idChuHo === item.idNhanKhau) && (
                                 <>
                                    <Button
                                       variant="danger"
                                       className={cx("me-2")}
                                       onClick={() => onRemoveItem(item, index)}
                                    >
                                       Xóa
                                    </Button>
                                    <Button
                                       variant="info"
                                       onClick={() =>
                                          handleOpenModalUpdate(item?.id ?? -1)
                                       }
                                    >
                                       Sửa
                                    </Button>
                                 </>
                              )}
                           </td>
                        </tr>
                     ))}
               </tbody>
            </Table>
         </div>
         <Modal show={showPopup} onHide={onClose} size="md" centered>
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>Xóa nhân khẩu nhân khẩu khỏi hộ</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               {/* <p>Ma ho hau :{residentR?.maHoKhau}</p> */}
               <p>Tên nhân khẩu : {residentR?.Resident?.hoTen}</p>
               ....
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Hủy
               </Button>
               <Button variant="danger" onClick={onSubmit}>
                  Xóa
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
}
export default React.memo(TableHouseholdDetail);
