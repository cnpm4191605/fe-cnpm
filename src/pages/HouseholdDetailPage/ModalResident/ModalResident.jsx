import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, ListGroup, Modal, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./ModalResident.module.scss";

import { typeModal as modalType, genderType } from "utils/constants.js";
import { convertDate, mapDateInput } from "utils/common.js";
import { useParams } from "react-router-dom";
import { useDebounce } from "hook";
import * as residentService from "services/residentService";
import {
   createMemberHousehold,
   updateMemberHouseholdById,
} from "services/memberHouseholdService";
import {
   getResidentListByHouseholdId,
   toggleModal,
} from "redux/resident/action";
const cx = classNames.bind(style);
function ModalMemberHousehold({ openModalAdd, handleCloseAddModal }) {
   const { typeModal, openModal, resident } = useSelector(
      (state) => state.resident
   );
   console.log("resident", resident);
   const { householdID } = useParams();
   const dispatch = useDispatch();

   const [searchResidentModal, setSearchResidentModal] = useState("");
   const [quanHeVoiChuHo, setQuanHeVoiChuHo] = useState("");
   const debounceValueModal = useDebounce(searchResidentModal, 500);
   const [showDropdown, setShowDropdown] = useState(false);
   const [residents, setResidents] = useState([]);
   const [residentT, setResident] = useState({});
   useEffect(() => {
      if (typeModal === modalType.UPDATE) {
         setResident({ ...resident });
         setQuanHeVoiChuHo(resident.quanHeVoiChuHo);
      }
   }, []);
   useEffect(() => {
      if (!debounceValueModal || debounceValueModal === "") {
         setShowDropdown(false);

         return;
      }
      const fetchResidentsList = async () => {
         try {
            let res = await residentService.getResidentListByFilter(
               debounceValueModal
            );
            setResidents(res.data);
         } catch (error) {
            console.log(error);
            setResidents([]);
            toast.error("Lấy danh sách nhân khẩu thất bại");
         }
      };
      fetchResidentsList();
      setShowDropdown(true);

      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [debounceValueModal]);

   const onClose = () => {
      setResident({});
      setQuanHeVoiChuHo("");
      setSearchResidentModal("");
      dispatch(
         toggleModal({
            typeModal: typeModal.CREATE,
            openModal: false,
         })
      );
   };
   const handleSelectedResident = (item) => {
      setResident({ ...item });
      // setSearchResidentModal(`${item?.hoTen}`);
      setShowDropdown(false);
   };
   const onSubmit = async () => {
      /// validate
      if (JSON.stringify(residentT) === "{}") {
         toast.error("Chưa chọn nhân khẩu");
         return;
      }
      if (typeModal === modalType.CREATE) {
         try {
            await createMemberHousehold({
               idNhanKhau: residentT.id,
               idHoKhau: householdID,
               quanHeVoiChuHo: quanHeVoiChuHo,
            });
            setQuanHeVoiChuHo("");

            setSearchResidentModal("");
            dispatch(
               toggleModal({
                  typeModal: typeModal.CREATE,
                  openModal: false,
               })
            );
            await dispatch(getResidentListByHouseholdId(householdID));
            toast.success("Thêm thành viên thành công");
         } catch (error) {
            console.log(error);
            toast.error("Thêm thành viên hộ thất bại");
         }
      }
      if (typeModal === modalType.UPDATE) {
         try {
            await updateMemberHouseholdById(residentT?.id, {
               idNhanKhau: residentT.idNhanKhau,
               idHoKhau: residentT.idHoKhau,
               quanHeVoiChuHo: quanHeVoiChuHo,
            });
            setQuanHeVoiChuHo("");

            setSearchResidentModal("");
            dispatch(
               toggleModal({
                  typeModal: typeModal.CREATE,
                  openModal: false,
               })
            );
            await dispatch(getResidentListByHouseholdId(householdID));
            toast.success("Chỉnh sửa thành công");
         } catch (error) {
            console.log(error);
            toast.error("Chỉnh sửa thất bại thất bại");
         }
      }
   };

   return (
      <Modal
         show={openModal}
         onHide={() => onClose}
         backdrop="static"
         size="lg"
         centered
      >
         <Modal.Header closeButton onHide={onClose}>
            <Modal.Title>
               {typeModal === "CREATE"
                  ? "Thêm thành viên hộ"
                  : "Chỉnh sửa quan hệ thành viên"}
            </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <Form>
               <Row>
                  <Col sm={12}>
                     <p>
                        {" "}
                        <b>Họ tên:&nbsp;&nbsp;</b>
                        {`${residentT?.hoTen ?? ""}`}
                     </p>
                  </Col>
                  <Col sm={12}>
                     <p>
                        <b>Số CMT:&nbsp;&nbsp;</b>
                        {`${residentT?.soCMT ?? ""}`}
                     </p>
                  </Col>
                  <Col sm={12}>
                     <p>
                        <b>Ngày sinh:&nbsp;&nbsp;</b>
                        {`${convertDate(residentT?.namSinh ?? "")}`}
                     </p>
                  </Col>
                  <Col sm={12}>
                     <p>
                        <b>Địa chỉ:&nbsp;&nbsp;</b>
                        {`${residentT?.diaChiHienNay ?? ""}`}
                     </p>
                  </Col>
                  {typeModal === modalType.CREATE && (
                     <Col lg={8} sm={8}>
                        <Form.Group className="mb-3" controlId="hoTen">
                           <Form.Label>Tìm kiếm</Form.Label>
                           <Form.Control
                              type="text"
                              placeholder="Tên / số CMT / địa chỉ"
                              autoComplete="off"
                              value={searchResidentModal}
                              onChange={(e) =>
                                 setSearchResidentModal(e.target.value)
                              }
                           />
                        </Form.Group>
                        {showDropdown && (
                           <ListGroup as="ul" className={cx("resident-list")}>
                              {residents.map((item) => (
                                 <ListGroup.Item
                                    action
                                    variant="light"
                                    // as="li"
                                    key={item.id}
                                    onClick={() => handleSelectedResident(item)}
                                    className={cx("resident-item")}
                                 >
                                    {`Họ tên: ${
                                       item?.hoTen ?? ""
                                    }, Ngày sinh: ${convertDate(
                                       item?.namSinh ?? ""
                                    )}, Địa chỉ: ${item?.diaChiHienNay}`}
                                 </ListGroup.Item>
                              ))}
                           </ListGroup>
                        )}
                     </Col>
                  )}

                  <Col lg={8} sm={8}>
                     <Form.Group className="mb-3" controlId="hoTen">
                        <Form.Label>Quan hệ với chủ hộ</Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Quan hệ với chủ hộ"
                           autoComplete="off"
                           value={quanHeVoiChuHo}
                           onChange={(e) => setQuanHeVoiChuHo(e.target.value)}
                        />
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}></Col>
               </Row>
            </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
               Đóng
            </Button>
            <Button variant="primary" onClick={onSubmit}>
               {typeModal === modalType.CREATE ? "Thêm" : "Sửa"}
            </Button>
         </Modal.Footer>
      </Modal>
   );
}
export default React.memo(ModalMemberHousehold);
