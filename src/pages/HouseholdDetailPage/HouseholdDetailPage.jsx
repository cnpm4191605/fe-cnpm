import { useSelector, useDispatch } from "react-redux";
import classNames from "classnames/bind";
import { Button, Container } from "react-bootstrap";
import styles from "./HouseholdDetail.module.scss";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getDetailHousehold } from "redux/household/action";
import { typeModal } from "utils/constants";
import BasePaging from "components/Paging/BasePaging";
import { useDebounce } from "hook";
import TableHouseholdDetail from "./TableHouseholdDetail/TableHouseholdDetail";
import ModalResident from "./ModalResident/ModalResident";
import { toggleModal } from "redux/resident/action";
import { toast } from "react-toastify";
import * as householdService from "services/householdService.js";
let cx = classNames.bind(styles);
function HouseholdDetailPage() {
   // const { household } = useSelector((state) => state.household);
   const { openModal } = useSelector((state) => state.resident);
   const { householdID } = useParams();
   // console.log(household, householdID);
   const dispatch = useDispatch();
   const [household, setHousehold] = useState({});
   useEffect(() => {
      const fetchApiHouseholdById = async () => {
         try {
            const res = await householdService.getHouseholdById(householdID);
            setHousehold({ ...res.data });
         } catch (error) {
            console.log(error);
            toast.error("Lấy thông tin hộ khẩu thất bại");
         }
      };
      fetchApiHouseholdById();
      // dispatch(getDetailHousehold(householdID));
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [householdID]);
   // const removeRow = useCallback(
   //    (id) => {
   //       dispatch(removeHousehold(id));
   //       // eslint-disable-next-line react-hooks/exhaustive-deps
   //    },
   //    [dispatch]
   // );
   const handleOpenCreateModal = () => {
      dispatch(
         toggleModal({
            typeModal: typeModal.CREATE,
            openModal: true,
         })
      );
   };
   return (
      <>
         <div className={cx("wrapper")}>
            <Container>
               <h3 className={cx("title")}>Thông tin chi tiết hộ khẩu</h3>
               <div className={cx("content")}>
                  <div className={cx("household-info", "mb-2")}>
                     <p>
                        {" "}
                        <b>Mã hộ khẩu:</b>
                        {household?.maHoKhau}{" "}
                     </p>
                     <p>
                        <b>Mã khu vực:</b> {household?.maKhuVuc}{" "}
                     </p>
                     <p>
                        <b>Tên chủ hộ:</b> {household?.householder?.hoTen}
                     </p>
                     <p>
                        <b>Số CMT:</b> {household?.householder?.soCMT}
                     </p>
                     <p>
                        <b>Địa chỉ:</b> {household?.diaChi}
                     </p>
                  </div>

                  <div className={cx("member-household")}>
                     <Button
                        variant="primary"
                        onClick={() => handleOpenCreateModal()}
                        className={cx("mb-2")}
                     >
                        Thêm mới nhân khẩu
                     </Button>
                     <TableHouseholdDetail household={household} />
                  </div>
               </div>
            </Container>
         </div>
         {openModal && <ModalResident />}
      </>
   );
}
export default HouseholdDetailPage;
