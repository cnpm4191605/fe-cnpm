import { Col, Container, Figure, Row } from "react-bootstrap";
import householdImg from "assets/images/household.png";
import residentImg from "assets/images/resident.png";
import temporaryResidenceImg from "assets/images/temporaryResidence.png";
import temporaryAbsentImg from "assets/images/temporaryAbsent.png";
import classNames from "classnames/bind";
import styles from "./HomePage.module.scss";
import { useEffect, useState } from "react";
import { countHousehold } from "services/householdService";
import { countResident } from "services/residentService";
import { countTemporaryAbsent } from "services/temporaryAbsentService";
import { countTemporaryResidence } from "services/temporaryResidenceService";
import CountUp from "react-countup";
import { toast } from "react-toastify";
const cx = classNames.bind(styles);
function HomePage() {
   const [househldCount, setHouseholdCount] = useState(0);
   const [residentCount, setResidentCount] = useState(0);
   const [temporaryResidenceCount, setTemporaryResidenceCount] = useState(0);
   const [temporaryAbsentCount, setTemporaryAbsentCount] = useState(0);
   useEffect(() => {
      const fetchApiCount = async () => {
         try {
            let household = await countHousehold();
            setHouseholdCount(household.data);
            let resident = await countResident();
            setResidentCount(resident.data);
            let temporaryAbsent = await countTemporaryAbsent();
            setTemporaryAbsentCount(temporaryAbsent.data);
            let temporaryResidence = await countTemporaryResidence();
            setTemporaryResidenceCount(temporaryResidence.data);
         } catch (error) {
            console.log(error);
            toast.error("Đã xảy ra lỗi xin vui lòng tải lại !!");
         }
      };
      fetchApiCount();
   }, []);
   return (
      <div>
         <Container fluid>
            <Row>
               <Col
                  sm={8}
                  className={cx("d-flex align-items-center mb-2", "home-item")}
               >
                  <Figure>
                     <Figure.Image
                        width={171}
                        height={180}
                        alt="171x180"
                        src={householdImg}
                     />
                  </Figure>
                  <div className={cx("ms-2", "home-item")}>
                     <h4>Số lượng hộ khẩu trên địa bàn:</h4>

                     <h3 className="text-center">
                        <CountUp start={0} end={househldCount} />
                     </h3>
                  </div>
               </Col>
               <Col
                  sm={8}
                  className={cx("d-flex align-items-center mb-2", "home-item")}
               >
                  <Figure>
                     <Figure.Image
                        width={171}
                        height={180}
                        alt="171x180"
                        src={residentImg}
                     />
                  </Figure>
                  <div className={cx("ms-2", "home-item")}>
                     <h4>Tổng số lượng nhân khẩu:</h4>

                     <h3 className="text-center">
                        <CountUp start={0} end={residentCount} />
                     </h3>
                  </div>
               </Col>
               <Col
                  sm={8}
                  className={cx("d-flex align-items-center mb-2", "home-item")}
               >
                  <Figure>
                     <Figure.Image
                        width={171}
                        height={180}
                        alt="171x180"
                        src={temporaryResidenceImg}
                     />
                  </Figure>
                  <div className={cx("ms-2", "home-item")}>
                     <h4>Số lượng nhân khẩu tạm trú:</h4>

                     <h3 className="text-center">
                        <CountUp start={0} end={temporaryResidenceCount} />
                     </h3>
                  </div>
               </Col>
               <Col
                  sm={8}
                  className={cx("d-flex align-items-center mb-2", "home-item")}
               >
                  <Figure>
                     <Figure.Image
                        width={171}
                        height={180}
                        alt="171x180"
                        src={temporaryAbsentImg}
                     />
                  </Figure>
                  <div className={cx("ms-2", "home-item")}>
                     <h4>Số lượng nhân khẩu tạm vắng:</h4>

                     <h3 className="text-center">
                        <CountUp start={0} end={setTemporaryAbsentCount} />
                     </h3>
                  </div>
               </Col>
            </Row>
         </Container>
      </div>
   );
}
export default HomePage;
