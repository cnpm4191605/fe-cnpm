import classNames from "classnames/bind";
import style from "./TableHousehold.module.scss";
import { Button, Modal, Table } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { convertDate } from "utils/common";
import { path, typeModal } from "utils/constants";
import { toggleModal, getDetailHousehold } from "redux/household/action";
const cx = classNames.bind(style);

function TableHousehold({ data, removeRow }) {
   const dispatch = useDispatch();
   const [showPopup, setShowPopup] = useState(false);
   const [household, setHousehold] = useState({});
   useEffect(() => {}, []);
   const onRemoveItem = (item) => {
      setShowPopup(true);
      setHousehold({ ...item });
   };
   const onClose = () => {
      setHousehold({});
      setShowPopup(false);
   };
   const onSubmit = () => {
      if (JSON.stringify(household) === "{}") {
      } else {
         removeRow(household.id);
      }
      setShowPopup(false);
   };
   const handleOpenModalUpdate = (householdId) => {
      dispatch(
         toggleModal({
            typeModal: typeModal.UPDATE,
            openModal: true,
         })
      );
      dispatch(getDetailHousehold(householdId));
   };
   console.log("re-render table");
   return (
      <>
         <div className={cx("table-container")}>
            <Table striped bordered hover>
               <thead className={cx("table-header")}>
                  <tr>
                     <th>STT</th>
                     <th>Mã hộ khẩu</th>
                     <th>Mã khu vực</th>
                     <th>Chủ hộ</th>
                     <th>Năm sinh</th>
                     <th>Giới tính</th>
                     <th>Số CMT</th>
                     <th>Địa chỉ</th>
                     <th>Chức năng</th>
                  </tr>
               </thead>
               <tbody>
                  {data &&
                     data.length > 0 &&
                     data.map((item, index) => (
                        <tr key={item.id}>
                           <td>{index + 1}</td>
                           <td>
                              <Link to={`${path.HOUSEHOLD}/${item.id}`}>
                                 <b>{item?.maHoKhau}</b>
                              </Link>
                           </td>
                           <td>{item?.maKhuVuc}</td>
                           <td>{item?.householder?.hoTen}</td>
                           <td>
                              {convertDate(
                                 item?.householder?.namSinh ?? "invalid date"
                              )}
                           </td>
                           <td>{item?.householder?.gioiTinh}</td>
                           <td>{item?.householder?.soCMT}</td>
                           <td>{item?.diaChi}</td>
                           <td>
                              <Button
                                 variant="danger"
                                 className={cx("me-2")}
                                 onClick={() => onRemoveItem(item, index)}
                              >
                                 Xóa
                              </Button>
                              <Button
                                 variant="info"
                                 onClick={() =>
                                    handleOpenModalUpdate(item?.id ?? -1)
                                 }
                              >
                                 Sửa
                              </Button>
                           </td>
                        </tr>
                     ))}
               </tbody>
            </Table>
         </div>
         <Modal show={showPopup} onHide={onClose} size="md" centered>
            <Modal.Header closeButton onHide={onClose}>
               <Modal.Title>Xóa hộ khẩu</Modal.Title>
            </Modal.Header>
            <Modal.Body>
               <p>Mã hộ khẩu :{household?.maHoKhau}</p>
               <p>Tên chủ hộ : {household?.householder?.hoTen}</p>
               ....
            </Modal.Body>
            <Modal.Footer>
               <Button variant="secondary" onClick={onClose}>
                  Hủy
               </Button>
               <Button variant="danger" onClick={onSubmit}>
                  Xóa
               </Button>
            </Modal.Footer>
         </Modal>
      </>
   );
}
export default React.memo(TableHousehold);
