import { useSelector, useDispatch } from "react-redux";
import classNames from "classnames/bind";
import { Button, Container } from "react-bootstrap";
import styles from "./Household.module.scss";
import { useCallback, useEffect, useState } from "react";
import {
   getHouseholdList,
   removeHousehold,
   toggleModal,
   setArgPage,
} from "redux/household/action";
import { typeModal } from "utils/constants";
// import * as householdService from "services/householdService";
// import { fieldHousehold, colHousehold } from "utils/constants";
import BasePaging from "components/Paging/BasePaging";
import { useDebounce } from "hook";
import CreateHouseholdModal from "./CreateHousehold/CreateHouseholdModal";
import TableHousehold from "./TableHousehold/TableHousehold";
let cx = classNames.bind(styles);
function HouseholdPage() {
   const { households, limit, totalRecords, offset, keyword } = useSelector(
      (state) => state.household
   );
   console.log(limit, totalRecords, offset);
   const [searchValue, setSearchValue] = useState("");
   const debounceValue = useDebounce(searchValue, 500);
   const dispatch = useDispatch();
   // const [showCreateModal, setShowCreateModal] = useState(false);
   useEffect(() => {
      dispatch(getHouseholdList());
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [limit, offset, keyword]);

   useEffect(() => {
      dispatch(
         setArgPage({
            limit,
            keyword: debounceValue,
            offset: 1,
         })
      );
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [debounceValue]);

   const onChangePage = useCallback(
      (newOffset) => {
         dispatch(
            setArgPage({
               limit,
               keyword,
               offset: newOffset,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword]
   );

   const onChangeLimit = useCallback(
      (mewLimit) => {
         dispatch(
            setArgPage({
               limit: mewLimit,
               keyword,
               offset: 1,
            })
         );
      },
      // eslint-disable-next-line react-hooks/exhaustive-deps
      [limit, offset, keyword]
   );

   // const onCloseCreateModal = useCallback(
   //    () => setShowCreateModal(false),
   //    // eslint-disable-next-line react-hooks/exhaustive-deps
   //    [showCreateModal]
   // );
   const removeRow = useCallback(
      (id) => {
         dispatch(removeHousehold(id));
         // eslint-disable-next-line react-hooks/exhaustive-deps
      },
      [dispatch]
   );
   const handleOpenCreateModal = () => {
      dispatch(
         toggleModal({
            typeModal: typeModal.CREATE,
            openModal: true,
         })
      );
   };
   return (
      <>
         <div className={cx("wrapper")}>
            <Container fluid>
               <h3 className={cx("title")}>Danh sách hộ khẩu khu dân cư </h3>
               <div className={cx("content")}>
                  <div className={cx("toolbars")}>
                     <div className={cx("search")}>
                        <Button
                           variant="primary"
                           onClick={() => handleOpenCreateModal()}
                        >
                           Thêm mới hộ khẩu
                        </Button>
                        <input
                           type="text"
                           className={cx("search-input")}
                           value={searchValue}
                           onChange={(e) =>
                              setSearchValue(
                                 e.target.value[0] === " " ? "" : e.target.value
                              )
                           }
                        />
                     </div>
                  </div>
                  <TableHousehold data={households} removeRow={removeRow} />
                  <BasePaging
                     limit={Number(limit)}
                     totalRecords={Number(totalRecords)}
                     offset={Number(offset)}
                     onChangePage={onChangePage}
                     onChangeLimit={onChangeLimit}
                  />
               </div>
            </Container>
         </div>
         <CreateHouseholdModal />
      </>
   );
}
export default HouseholdPage;
