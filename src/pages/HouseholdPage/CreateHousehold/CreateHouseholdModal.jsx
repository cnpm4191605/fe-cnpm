import classNames from "classnames/bind";
import React, { useState, useEffect } from "react";
import { Button, Col, Form, Modal, Row, ListGroup } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import style from "./CreateHouseholdModal.module.scss";
import {
   createHousehold,
   toggleModal,
   updateHousehold,
} from "redux/household/action";
import { useDebounce } from "hook";
import * as residentService from "services/residentService.js";
import { typeModal as modalType } from "utils/constants.js";
const cx = classNames.bind(style);
function CreateHouseholdModal(/*{ show, typeModal }*/) {
   const { typeModal, openModal, household } = useSelector(
      (state) => state.household
   );
   const dispatch = useDispatch();
   const [newHousehold, setNewHousehold] = useState({});
   const [showDropdown, setShowDropdown] = useState(false);
   const [residents, setResidents] = useState([]);
   const [searchHouseholder, setSearchHouseholder] = useState("");
   const debounceValue = useDebounce(searchHouseholder, 500);
   const [reloadResident, setReloadResident] = useState(true);
   const [errors, setErrors] = useState({});

   // console.log("check newHousehold:", newHousehold);

   useEffect(() => {
      if (typeModal === modalType.CREATE) {
         setSearchHouseholder("");
         return;
      }
      if (typeModal === modalType.UPDATE) {
         console.log("check household:", household);
         setNewHousehold({ ...household });
         setSearchHouseholder(household?.householder?.hoTen ?? "");
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [household]);
   useEffect(() => {
      if (reloadResident) {
         const fetchApiResidents = async () => {
            try {
               if (debounceValue.trim() === " ") return;
               let res = await residentService.getResidentListByFilter(
                  debounceValue
               );
               setResidents(res?.data ?? []);
            } catch (error) {
               console.log(error);
               toast.error("lay danh sach nhan khau that bai");
               setResidents([]);
            }
         };
         fetchApiResidents();
         setNewHousehold({
            ...newHousehold,
            idChuHo: -1,
         });
         // setShowDropdown(true);
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [debounceValue]);

   const onChangeInput = (e, field) => {
      setNewHousehold({
         ...newHousehold,
         [field]: e.target.value,
      });
      if (!!errors[field])
         setErrors({
            ...errors,
            [field]: null,
         });
   };
   const onclickHouseholder = (resident) => {
      setNewHousehold({
         ...newHousehold,
         idChuHo: resident.id,
      });
      if (!!errors.idChuHo) {
         setErrors({
            ...errors,
            idChuHo: null,
         });
      }
      setSearchHouseholder(`${resident.hoTen}`);
      setShowDropdown(false);
      setReloadResident(false);
   };
   const onChangeHouseholder = (e) => {
      setSearchHouseholder(e.target.value);
      setReloadResident(true);
      setShowDropdown(true);
   };
   const onClose = () => {
      setNewHousehold({});
      dispatch(
         toggleModal({
            typeModal: modalType.CREATE,
            openModal: false,
         })
      );
      setShowDropdown(false);
   };
   const findFormErrors = () => {
      const { maHoKhau, maKhuVuc, diaChi, idChuHo } = newHousehold;
      const newErrors = {};
      if (!maHoKhau || maHoKhau === "")
         newErrors.maHoKhau = "Mã hộ khẩu không được để trống";
      if (!maKhuVuc || maKhuVuc === "")
         newErrors.maKhuVuc = "Mã khu vực không được để trống";
      if (!diaChi || diaChi === "")
         newErrors.diaChi = "Địa chỉ không được để trống";
      if (!idChuHo || idChuHo === -1) {
         newErrors.idChuHo = "Chưa chọn chủ hộ";
      }
      return newErrors;
   };
   const onSubmit = () => {
      const newErrors = findFormErrors();
      // Conditional logic:
      if (Object.keys(newErrors).length > 0) {
         // We got errors!
         setErrors(newErrors);
         return;
      }
      setShowDropdown(false);
      if (typeModal === modalType.CREATE) {
         dispatch(createHousehold(newHousehold));
      }
      if (typeModal === modalType.UPDATE) {
         dispatch(
            updateHousehold(newHousehold.id, {
               maHoKhau: newHousehold.maHoKhau,
               idChuHo: newHousehold.idChuHo,
               maKhuVuc: newHousehold.maKhuVuc,
               diaChi: newHousehold.diaChi,
               ngayChuyenDi: newHousehold.ngayChuyenDi,
               lyDoChuyen: newHousehold.lyDoChuyen,
            })
         );
      }
   };
   return (
      <Modal
         show={openModal}
         onHide={onClose}
         backdrop="static"
         size="lg"
         centered
      >
         <Modal.Header closeButton onHide={onClose}>
            <Modal.Title>
               {typeModal === modalType.UPDATE
                  ? "Cập nhật hộ khẩu"
                  : "Thêm hộ khẩu mới"}
            </Modal.Title>
         </Modal.Header>
         <Modal.Body>
            <Form>
               <Row>
                  <Col lg={6} sm={12}>
                     <Form.Group
                        className="mb-3"
                        controlId="formCreateHouseholdCode"
                     >
                        <Form.Label>
                           Mã hộ khẩu<span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Mã khẩu khẩu"
                           value={newHousehold?.maHoKhau ?? ""}
                           onChange={(e) => onChangeInput(e, "maHoKhau")}
                           isInvalid={!!errors.maHoKhau}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.maHoKhau}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={6} sm={12}>
                     <Form.Group className="mb-3" controlId="maKhuVuc">
                        <Form.Label>
                           Mã khu vực<span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Mã khu vực"
                           value={newHousehold?.maKhuVuc ?? ""}
                           onChange={(e) => onChangeInput(e, "maKhuVuc")}
                           isInvalid={!!errors.maKhuVuc}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.maKhuVuc}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>
                  <Col lg={12} sm={12}>
                     <Form.Group
                        className="mb-3"
                        controlId="formCreateHouseholdAddressHouse"
                     >
                        <Form.Label>
                           Chủ hộ <span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="text"
                           placeholder=" chủ hộ/ số CMT"
                           value={searchHouseholder}
                           onChange={(e) => onChangeHouseholder(e)}
                           onClick={() => setShowDropdown(!showDropdown)}
                           autoComplete="off"
                           isInvalid={!!errors.idChuHo}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.idChuHo}
                        </Form.Control.Feedback>
                        {showDropdown && (
                           <ListGroup as="ul" className={cx("resident-list")}>
                              {residents.map((resident) => (
                                 <ListGroup.Item
                                    action
                                    variant="light"
                                    // as="li"
                                    key={resident.id}
                                    onClick={() => onclickHouseholder(resident)}
                                    className={cx(
                                       "resident-item",
                                       `${
                                          resident.id ===
                                             newHousehold?.idChuHo ?? -1
                                             ? "selected"
                                             : ""
                                       }`
                                    )}
                                 >
                                    {`mã nhân khẩu: ${
                                       resident?.maNhanKhau ?? ""
                                    }, họ tên: ${
                                       resident?.hoTen ?? ""
                                    }, sô CMT: ${resident?.soCMT ?? ""}`}
                                 </ListGroup.Item>
                              ))}
                           </ListGroup>
                        )}
                     </Form.Group>
                  </Col>
                  <Col lg={12} sm={12}>
                     <Form.Group className="mb-3" controlId="diaChi">
                        <Form.Label>
                           Địa chỉ <span className="text-danger">(*)</span>
                        </Form.Label>
                        <Form.Control
                           type="text"
                           placeholder="Địa chỉ"
                           value={newHousehold?.diaChi ?? ""}
                           onChange={(e) => onChangeInput(e, "diaChi")}
                           isInvalid={!!errors.diaChi}
                        />
                        <Form.Control.Feedback type="invalid">
                           {errors.diaChi}
                        </Form.Control.Feedback>
                     </Form.Group>
                  </Col>

                  <Col lg={6} sm={12}></Col>
               </Row>
            </Form>
         </Modal.Body>
         <Modal.Footer>
            <Button variant="secondary" onClick={onClose}>
               Đóng
            </Button>
            <Button variant="primary" onClick={onSubmit}>
               {typeModal === modalType.UPDATE ? "Cập nhật" : "Thêm mới"}
            </Button>
         </Modal.Footer>
      </Modal>
   );
}
export default React.memo(CreateHouseholdModal);
