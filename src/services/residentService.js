import request from "../utils/httpRequest";

const getResidentList = async (
   limit = 20,
   offset = 1,
   keyword = "",
   minAge = 0,
   maxAge = 150,
   gender = "",
   status = ""
) => {
   try {
      const res = await request.get(
         `/api/residents?limit=${limit}&offset=${offset}&keyword=${keyword}&minAge=${minAge}&maxAge=${maxAge}&gender=${gender}&status=${status}`
      );

      return res.data;
   } catch (error) {
      throw error;
   }
};
const getResidentListByFilter = async (keyword = "") => {
   try {
      let res = await request.get(`/api/residents/filter?keyword=${keyword}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getResidentById = async (residentId) => {
   try {
      let res = await request.get(`/api/residents/${residentId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const createResident = async (resident) => {
   try {
      let res = await request.post("/api/residents/", resident);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const deleteResidentById = async (residentId) => {
   try {
      let res = await request.delete(`/api/residents/${residentId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const updateResidentById = async (residentId, updateBody) => {
   try {
      let res = await request.put(`/api/residents/${residentId}`, updateBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const countResident = async () => {
   try {
      let res = await request.get(`/api/residents/count`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export {
   countResident,
   getResidentList,
   createResident,
   deleteResidentById,
   getResidentListByFilter,
   getResidentById,
   updateResidentById,
};
