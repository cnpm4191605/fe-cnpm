import request from "../utils/httpRequest";

const getMeetingList = async () => {
   try {
      let res = await request.get(`/api/meetings`);

      return res.data;
   } catch (error) {
      throw error;
   }
};
const createNewMeeting = async (newMeeting) => {
   try {
      let res = await request.post("/api/meetings", newMeeting);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const deleteMeetingById = async (householdId) => {
   try {
      let res = await request.delete(`/api/meetings/${householdId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getMeetingById = async (id) => {
   try {
      let res = await request.get(`api/meetings/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getMeetingByHouseholdId = async (householdId) => {
   try {
      let res = await request.get(`api/meetings/household/${householdId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};

const createMeetingHousehold = async (createBody) => {
   try {
      let res = await request.post(`api/meeting-household`, createBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export {
   getMeetingList,
   createNewMeeting,
   deleteMeetingById,
   getMeetingById,
   getMeetingByHouseholdId,
   createMeetingHousehold,
};
