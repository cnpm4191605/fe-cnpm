import request from "../utils/httpRequest";

// const getHouseholdList = async (limit = 20, offset = 1, keyword = "") => {
//    try {
//       let res = await request.get(
//          `/api/households?limit=${limit}&offset=${offset}&keyword=${keyword}`
//       );
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
const createTemporaryAbsent = async (createBody) => {
   try {
      let res = await request.post("/api/temporary-absent", createBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
// const deleteHouseholdById = async (householdId) => {
//    try {
//       let res = await request.delete(`/api/households/${householdId}`);
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
// const updateHouseholdById = async (householdId, updateBody) => {
//    try {
//       let res = await request.put(`api/households/${householdId}`, updateBody);
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
const getTemporaryAbsent = async (id) => {
   try {
      let res = await request.get(`api/temporary-absent/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const countTemporaryAbsent = async () => {
   try {
      let res = await request.get(`api/temporary-absent/count`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export { createTemporaryAbsent, getTemporaryAbsent, countTemporaryAbsent };
