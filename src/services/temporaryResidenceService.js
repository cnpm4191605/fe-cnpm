import request from "../utils/httpRequest";

// const getHouseholdList = async (limit = 20, offset = 1, keyword = "") => {
//    try {
//       let res = await request.get(
//          `/api/households?limit=${limit}&offset=${offset}&keyword=${keyword}`
//       );
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
const createTemporaryResidence = async (createBody) => {
   try {
      let res = await request.post("/api/temporary-residence", createBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
// const deleteHouseholdById = async (householdId) => {
//    try {
//       let res = await request.delete(`/api/households/${householdId}`);
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
// const updateHouseholdById = async (householdId, updateBody) => {
//    try {
//       let res = await request.put(`api/households/${householdId}`, updateBody);
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
const getTemporaryResidence = async (id) => {
   try {
      let res = await request.get(`api/temporary-residence/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const countTemporaryResidence = async () => {
   try {
      let res = await request.get(`api/temporary-residence/count`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export {
   createTemporaryResidence,
   getTemporaryResidence,
   countTemporaryResidence,
};
