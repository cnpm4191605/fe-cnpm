import request from "../utils/httpRequest";

// const getAllHousehold = async () => {
//    try {
//       let res = await request.get("/api/households");
//       return res.data;
//    } catch (error) {
//       console.log(error);
//       return error.response.data;
//    }
// };
const getHouseholdList = async (
   limit = 20,
   offset = 1,
   keyword = "",
   meetingId
) => {
   try {
      let res = {};
      if (meetingId) {
         res = await request.get(
            `/api/households?limit=${limit}&offset=${offset}&keyword=${keyword}&meetingId=${meetingId}`
         );
      } else {
         res = await request.get(
            `/api/households?limit=${limit}&offset=${offset}&keyword=${keyword}`
         );
      }

      return res.data;
   } catch (error) {
      throw error;
   }
};
const createNewHousehold = async (newHousehold) => {
   try {
      let res = await request.post("/api/households", newHousehold);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const deleteHouseholdById = async (householdId) => {
   try {
      let res = await request.delete(`/api/households/${householdId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const updateHouseholdById = async (householdId, updateBody) => {
   try {
      let res = await request.put(`api/households/${householdId}`, updateBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getHouseholdById = async (householdId) => {
   try {
      let res = await request.get(`api/households/${householdId}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const countHousehold = async () => {
   try {
      let res = await request.get(`api/households/count`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export {
   countHousehold,
   getHouseholdList,
   createNewHousehold,
   deleteHouseholdById,
   updateHouseholdById,
   getHouseholdById,
};
