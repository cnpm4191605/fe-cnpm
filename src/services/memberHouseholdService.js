import request from "../utils/httpRequest";

// const getHouseholdList = async (limit = 20, offset = 1, keyword = "") => {
//    try {
//       let res = await request.get(
//          `/api/households?limit=${limit}&offset=${offset}&keyword=${keyword}`
//       );
//       return res.data;
//    } catch (error) {
//       throw error;
//    }
// };
const createMemberHousehold = async (createBody) => {
   try {
      let res = await request.post("/api/member-household", createBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const deleteMemberHouseholdByResidentId = async (id) => {
   try {
      let res = await request.delete(`api/member-household/resident/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const updateMemberHouseholdById = async (id, updateBody) => {
   try {
      let res = await request.put(`api/member-household/${id}`, updateBody);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getMemberHouseholdByHouseholdId = async (id) => {
   try {
      let res = await request.get(`api/member-household/household/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
const getMemberHouseholdById = async (id) => {
   try {
      let res = await request.get(`api/member-household/${id}`);
      return res.data;
   } catch (error) {
      throw error;
   }
};
export {
   getMemberHouseholdById,
   createMemberHousehold,
   getMemberHouseholdByHouseholdId,
   deleteMemberHouseholdByResidentId,
   updateMemberHouseholdById,
};
