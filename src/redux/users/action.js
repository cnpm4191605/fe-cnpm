import { toast } from "react-toastify";
import actionTypes from "./actionTypes";
import * as userService from "services/userService.js";
import { localStore } from "utils/constants";
export const login = (username, password) => {
   return async (dispatch, getState) => {
      try {
         let res = await userService.login(username, password);
         dispatch({
            type: actionTypes.LOGIN_SUCCESS,
            payload: {
               userInfo: res.data?.user,
               token: res.data?.token,
            },
         });
         localStorage.setItem(localStore.TOKEN, JSON.stringify(res.data.token));
         localStorage.setItem(localStore.USER, JSON.stringify(res.data.user));
         window.location.reload();
      } catch (error) {
         console.log(error);
         let msgError =
            error?.response?.data?.message?.details[0]?.message ||
            error?.response?.data?.message;
         dispatch({ type: actionTypes.LOGIN_FAIL, payload: msgError });
      }
   };
};

export const logout = () => (dispatch) => {
   localStorage.removeItem(localStore.USER);
   localStorage.removeItem(localStore.TOKEN);

   dispatch({
      type: actionTypes.LOGOUT,
   });
};
