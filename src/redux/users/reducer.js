import actionTypes from "./actionTypes";
import { localStore } from "utils/constants";
const initialState = {
   userInfo: null,
   token: null,
   message: "",
};

const userReducer = (state = initialState, action) => {
   switch (action.type) {
      case actionTypes.LOGIN_SUCCESS:
         return {
            userInfo: action.payload.user,
            token: action.payload.token,
            message: "",
         };
      case actionTypes.LOGIN_FAIL:
         return {
            userInfo: null,
            token: null,
            message: action.payload,
         };

      case actionTypes.LOGOUT:
         return {
            userInfo: null,
            token: null,
            message: "",
         };

      default:
         return state;
   }
};

export default userReducer;
