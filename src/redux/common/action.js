import * as actionTypes from "./actionTypes.js";

export const loadingWait = () => (dispatch, getState) => {
   return dispatch({ type: actionTypes.LOADING_WAIT });
};
export const loadingFinished = () => (dispatch, getState) => {
   return dispatch({ type: actionTypes.LOADING_FINISHED });
};
export const toggleSidebar = () => (dispatch, getState) => {
   return dispatch({ type: actionTypes.TOGGLE_SIDEBAR });
};
export const setArgPage = (payload) => (dispatch) => {
   dispatch({
      type: actionTypes.PAGING_FILTER,
      payload: { ...payload },
   });
};
