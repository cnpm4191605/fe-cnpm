import * as actionTypes from "./actionTypes.js";
const initState = {
   loading: false,
   toggleSidebar: false,
};
const commonReducer = (state = initState, action) => {
   switch (action.type) {
      case actionTypes.LOADING_WAIT:
         return {
            ...state,
            loading: true,
         };
      case actionTypes.LOADING_FINISHED:
         return {
            ...state,
            loading: false,
         };
      case actionTypes.TOGGLE_SIDEBAR:
         return {
            ...state,
            toggleSidebar: !state.toggleSidebar,
         };
      default:
         return state;
   }
};
export default commonReducer;
