import { toast } from "react-toastify";
import actionTypes from "./actionTypes";
import * as residentService from "services/residentService";
import * as memberHouseholdService from "services/memberHouseholdService.js";
import * as commonTypes from "../common/actionTypes.js";
import { typeModal } from "utils/constants";
export const getResidentList = () => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         const {
            limit = 20,
            offset = 1,
            keyword = "",
            minAge = 0,
            maxAge = 150,
            gender = "",
            status = "",
         } = getState().resident;
         let res = await residentService.getResidentList(
            limit,
            offset,
            keyword,
            minAge,
            maxAge,
            gender,
            status
         );
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_LIST_RESIDENT_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_LIST_RESIDENT_FAIL,
         });

         toast.error("Lấy danh sách nhân khẩu thất bại !!");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const setArgPage = (payload) => (dispatch) => {
   dispatch({
      type: actionTypes.PAGING_FILTER,
      payload: { ...payload },
   });
};

export const removeResident = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await residentService.deleteResidentById(id);
         await memberHouseholdService.deleteMemberHouseholdByResidentId(
            res.data
         );
         dispatch({
            type: actionTypes.DELETE_RESIDENT_SUCCESS,
            payload: Number(res.data),
         });
         toast.success("Xóa nhân khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Xóa nhân khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const createResident = (createBody) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await residentService.createResident(createBody);
         dispatch({
            type: actionTypes.CREATE_RESIDENT_SUCCESS,
            payload: res.data,
         });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         toast.success("Thêm hộ khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Thêm hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const createResidentInHousehold = (createBody, memberHousehold) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await residentService.createResident(createBody);
         await memberHouseholdService.createMemberHousehold({
            idNhanKhau: res.data?.id,
            idHoKhau: memberHousehold.idHoKhau,
            quanHeVoiChuHo: memberHousehold.quanHeVoiChuHo,
         });
         // dispatch({
         //    type: actionTypes.CREATE_RESIDENT_SUCCESS,
         //    payload: res.data,
         // });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         window.location.reload();
         toast.success("Thêm hộ khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Thêm hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const updateResidentById = (residentId, updateBody) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await residentService.updateResidentById(
            residentId,
            updateBody
         );
         dispatch({
            type: actionTypes.UPDATE_RESIDENT_SUCCESS,
            payload: res.data,
         });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         toast.success("Chỉnh sửa nhân khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Chỉnh sửa nhân khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const updateResidentOfMemberHousehold = (
   residentId,
   updateBody,
   updateMemberHousehold
) => {};
export const getResidentById = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });

         let res = await residentService.getResidentById(id);
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_RESIDENT_BY_ID_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         toast.error("Lấy thông tin nhân khẩu thất bại");
         dispatch({
            type: actionTypes.GET_RESIDENT_BY_ID_FAIL,
         });
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};

export const getResidentListByHouseholdId = (householdId) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await memberHouseholdService.getMemberHouseholdByHouseholdId(
            householdId
         );
         if (res && res.code === 200) {
            // let arrList = res.data.map((item) => ({
            //    idHoKhau: item.idHoKhau,
            //    quanHeVoiChuHo: item.quanHeVoiChuHo,
            //    ...(item?.Resident ?? {}),
            // }));
            dispatch({
               type: actionTypes.GET_LIST_RESIDENT_SUCCESS,
               payload: {
                  items: res.data,
                  totalItems: res.data.length,
               },
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_LIST_RESIDENT_FAIL,
         });

         toast.error("Lấy danh sách nhân khẩu thất bại !!");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const getMemberHouseholdById = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await memberHouseholdService.getMemberHouseholdById(id);
         if (res && res.code === 200) {
            let newObj = {
               id: res.data.id,
               idNhanKhau: res.data.idNhanKhau,
               idHoKhau: res.data.idHoKhau,
               quanHeVoiChuHo: res.data.quanHeVoiChuHo,
               ...(res.data?.Resident ?? {}),
            };
            dispatch({
               type: actionTypes.GET_RESIDENT_BY_ID_SUCCESS,
               payload: newObj,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_RESIDENT_BY_ID_FAIL,
         });

         toast.error("Lấy thông tin nhân khẩu thất bại !!");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const toggleModal = (payload) => (dispatch) => {
   return dispatch({
      type: actionTypes.TOGGLE_MODAL,
      payload,
   });
};
export const setResidentIdSelected = (payload) => (dispatch) => {
   return dispatch({
      type: actionTypes.SET_RESIDENT_ID_SELECT,
      payload,
   });
};
export const toggleDeclaration = (payload) => (dispatch) => {
   return dispatch({
      type: actionTypes.TOGGLE_DECLARATION,
      payload,
   });
};
