import { toast } from "react-toastify";
import actionTypes from "./actionTypes";
import * as householdService from "services/householdService.js";
import * as memberHouseholdService from "services/memberHouseholdService.js";
import * as commonTypes from "../common/actionTypes.js";
import { typeModal } from "utils/constants";
export const getHouseholdList = (meetingId) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         // dispatch({
         //    type: actionTypes.GET_LIST_HOUSEHOLD_REQUEST,
         // });
         const { limit = 20, offset = 1, keyword = "" } = getState().household;

         let res = await householdService.getHouseholdList(
            limit,
            offset,
            keyword,
            meetingId
         );
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_LIST_HOUSEHOLD_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_LIST_HOUSEHOLD_FAIL,
         });

         toast.error("Lấy danh sách hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const setArgPage = (payload) => (dispatch) => {
   dispatch({
      type: actionTypes.PAGING_FILTER,
      payload: { ...payload },
   });
};

export const removeHousehold = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await householdService.deleteHouseholdById(id);
         dispatch({
            type: actionTypes.DELETE_HOUSEHOLD_SUCCESS,
            payload: Number(res.data),
         });
         toast.success("Xóa hộ khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Xóa hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const createHousehold = (createBody) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await householdService.createNewHousehold(createBody);
         await memberHouseholdService.createMemberHousehold({
            idNhanKhau: res.data?.idChuHo,
            idHoKhau: res.data?.id,
            quanHeVoiChuHo: "chủ hộ",
         });
         dispatch({
            type: actionTypes.CREATE_HOUSEHOLD_SUCCESS,
            payload: res.data,
         });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         toast.success("Thêm hộ khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Thêm hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const updateHousehold = (householdId, updateBody) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await householdService.updateHouseholdById(
            householdId,
            updateBody
         );
         // chưa sửa lại thành viên trong hộ ??
         dispatch({
            type: actionTypes.UPDATE_HOUSEHOLD_SUCCESS,
            payload: res.data,
         });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         toast.success("Chỉnh sửa hộ khẩu thành công");
      } catch (error) {
         console.log(error);
         toast.error("Chỉnh sửa hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const getDetailHousehold = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });

         let res = await householdService.getHouseholdById(id);
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_HOUSEHOLD_BY_ID_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_HOUSEHOLD_BY_ID_FAIL,
         });
         toast.error("Lấy thông tin hộ khẩu thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const toggleModal = (payload) => (dispatch) => {
   return dispatch({
      type: actionTypes.TOGGLE_MODAL,
      payload,
   });
};
