import actionTypes from "./actionTypes.js";
import { typeModal } from "utils/constants.js";
const initialState = {
   households: [],
   household: {},
   limit: 20,
   offset: 1,
   keyword: "",
   totalRecords: 0,
   typeModal: typeModal.CREATE,
   openModal: false,
};

const householdReducer = (state = initialState, action) => {
   switch (action.type) {
      case actionTypes.GET_LIST_HOUSEHOLD_SUCCESS:
         return {
            ...state,
            households: [...action.payload.items],
            totalRecords: action.payload.totalItems,
         };
      case actionTypes.GET_LIST_HOUSEHOLD_FAIL:
         return {
            ...state,
            households: [],
            totalRecords: 0,
         };

      case actionTypes.GET_HOUSEHOLD_BY_ID_SUCCESS:
         return {
            ...state,
            household: { ...action.payload },
         };
      case actionTypes.GET_HOUSEHOLD_BY_ID_FAIL:
         return {
            ...state,
            household: {},
         };

      // create household
      case actionTypes.CREATE_HOUSEHOLD_SUCCESS:
         return {
            ...state,
            totalRecords: state.totalRecords + 1,
            // household: { ...action.payload },
            households: [{ ...action.payload }, ...state.households],
         };

      // set arg paging
      case actionTypes.PAGING_FILTER:
         return {
            ...state,
            offset: action.payload.offset,
            keyword: action.payload.keyword,
            limit: action.payload.limit,
         };

      // delete househld
      case actionTypes.DELETE_HOUSEHOLD_SUCCESS:
         const arrayDelete = [...state.households];
         let idxDelete = arrayDelete.findIndex(
            (household) => household.id === action.payload
         );
         arrayDelete.splice(idxDelete, 1);
         if (arrayDelete.length === 0) {
            return {
               ...state,
               offset: state.offset === 1 ? 1 : state.offset - 1,
               totalRecords: state.totalRecords - 1,
               households: [...arrayDelete],
            };
         } else {
            return {
               ...state,
               totalRecords: state.totalRecords - 1,
               households: [...arrayDelete],
            };
         }

      // update househld
      case actionTypes.UPDATE_HOUSEHOLD_SUCCESS:
         const arrayUpdate = [...state.households];
         let idxUpdate = arrayUpdate.findIndex(
            (household) => household.id === action.payload.id
         );
         if (idxUpdate !== -1) {
            arrayUpdate[idxUpdate] = { ...action.payload };
            return {
               ...state,
               households: [...arrayUpdate],
            };
         }
         return state;

      // toggle modal
      case actionTypes.TOGGLE_MODAL:
         return {
            ...state,
            household: {},
            typeModal: action.payload.typeModal,
            openModal: action.payload.openModal,
         };
      default:
         return state;
   }
};

export default householdReducer;
