import { toast } from "react-toastify";
import actionTypes from "./actionTypes";
import * as meetingService from "services/meetingService.js";
import * as memberHouseholdService from "services/memberHouseholdService.js";
import * as commonTypes from "../common/actionTypes.js";
import { typeModal } from "utils/constants";
export const getListMeeting = () => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         // const { limit = 20, offset = 1, keyword = "" } = getState().household;

         let res = await meetingService.getMeetingList();
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_LIST_MEETING_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_LIST_MEETING_FAIL,
         });

         toast.error("Lấy danh sách thư họp thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
// export const setArgPage = (payload) => (dispatch) => {
//    dispatch({
//       type: actionTypes.PAGING_FILTER,
//       payload: { ...payload },
//    });
// };

// export const removeHousehold = (id) => {
//    return async (dispatch, getState) => {
//       try {
//          dispatch({
//             type: commonTypes.LOADING_WAIT,
//          });
//          let res = await meetingService.deleteHouseholdById(id);
//          dispatch({
//             type: actionTypes.DELETE_HOUSEHOLD_SUCCESS,
//             payload: Number(res.data),
//          });
//          toast.success("Xóa hộ khẩu thành công");
//       } catch (error) {
//          console.log(error);
//          toast.error("Xóa hộ khẩu thất bại");
//       } finally {
//          dispatch({
//             type: commonTypes.LOADING_FINISHED,
//          });
//       }
//    };
// };
export const createMeeting = (createBody) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });
         let res = await meetingService.createNewMeeting(createBody);
         dispatch({
            type: actionTypes.CREATE_MEETING_SUCCESS,
            payload: res.data,
         });
         dispatch({
            type: actionTypes.TOGGLE_MODAL,
            payload: {
               typeModal: typeModal.CREATE,
               openModal: false,
            },
         });
         toast.success("Thêm thư họp thành công");
      } catch (error) {
         console.log(error);
         toast.error("Thêm thư họp thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
// export const updateHousehold = (householdId, updateBody) => {
//    return async (dispatch, getState) => {
//       try {
//          dispatch({
//             type: commonTypes.LOADING_WAIT,
//          });
//          let res = await meetingService.updateHouseholdById(
//             householdId,
//             updateBody
//          );
//          // chưa sửa lại thành viên trong hộ ??
//          dispatch({
//             type: actionTypes.UPDATE_HOUSEHOLD_SUCCESS,
//             payload: res.data,
//          });
//          dispatch({
//             type: actionTypes.TOGGLE_MODAL,
//             payload: {
//                typeModal: typeModal.CREATE,
//                openModal: false,
//             },
//          });
//          toast.success("Chỉnh sửa hộ khẩu thành công");
//       } catch (error) {
//          console.log(error);
//          toast.error("Chỉnh sửa hộ khẩu thất bại");
//       } finally {
//          dispatch({
//             type: commonTypes.LOADING_FINISHED,
//          });
//       }
//    };
// };
export const getMeetingById = (id) => {
   return async (dispatch, getState) => {
      try {
         dispatch({
            type: commonTypes.LOADING_WAIT,
         });

         let res = await meetingService.getMeetingById(id);
         if (res && res.code === 200) {
            dispatch({
               type: actionTypes.GET_MEETING_BY_ID_SUCCESS,
               payload: res.data,
            });
         }
      } catch (error) {
         console.log("error", error);
         dispatch({
            type: actionTypes.GET_MEETING_BY_ID_FAIL,
         });
         toast.error("Lấy thông tin cuộc họp thất bại");
      } finally {
         dispatch({
            type: commonTypes.LOADING_FINISHED,
         });
      }
   };
};
export const toggleModal = (payload) => (dispatch) => {
   return dispatch({
      type: actionTypes.TOGGLE_MODAL,
      payload,
   });
};
