import actionTypes from "./actionTypes.js";
import { typeModal } from "utils/constants.js";
const initialState = {
   meetings: [],
   meeting: {},
};

const meetingReducer = (state = initialState, action) => {
   switch (action.type) {
      case actionTypes.GET_LIST_MEETING_SUCCESS:
         return {
            ...state,
            meetings: [...action.payload],
         };
      case actionTypes.GET_LIST_MEETING_FAIL:
         return {
            ...state,
            meetings: [],
         };

      case actionTypes.GET_MEETING_BY_ID_SUCCESS:
         return {
            ...state,
            meeting: { ...action.payload },
         };
      case actionTypes.GET_MEETING_BY_ID_FAIL:
         return {
            ...state,
            meeting: {},
         };

      // create meeting
      case actionTypes.CREATE_MEETING_SUCCESS:
         return {
            ...state,
            meetings: [{ ...action.payload }, ...state.meetings],
         };

      // set arg paging
      // case actionTypes.PAGING_FILTER:
      //    return {
      //       ...state,
      //       offset: action.payload.offset,
      //       keyword: action.payload.keyword,
      //       limit: action.payload.limit,
      //    };

      // delete househld
      // case actionTypes.DELETE_HOUSEHOLD_SUCCESS:
      //    const arrayDelete = [...state.meetings];
      //    let idxDelete = arrayDelete.findIndex(
      //       (meeting) => meeting.id === action.payload
      //    );
      //    arrayDelete.splice(idxDelete, 1);
      //    if (arrayDelete.length === 0) {
      //       return {
      //          ...state,
      //          offset: state.offset === 1 ? 1 : state.offset - 1,
      //          totalRecords: state.totalRecords - 1,
      //          meetings: [...arrayDelete],
      //       };
      //    } else {
      //       return {
      //          ...state,
      //          totalRecords: state.totalRecords - 1,
      //          meetings: [...arrayDelete],
      //       };
      //    }

      // update househld
      // case actionTypes.UPDATE_HOUSEHOLD_SUCCESS:
      //    const arrayUpdate = [...state.meetings];
      //    let idxUpdate = arrayUpdate.findIndex(
      //       (meeting) => meeting.id === action.payload.id
      //    );
      //    if (idxUpdate !== -1) {
      //       arrayUpdate[idxUpdate] = { ...action.payload };
      //       return {
      //          ...state,
      //          meetings: [...arrayUpdate],
      //       };
      //    }
      //    return state;

      // toggle modal
      case actionTypes.TOGGLE_MODAL:
         return {
            ...state,
            meeting: {},
            typeModal: action.payload.typeModal,
            openModal: action.payload.openModal,
         };
      default:
         return state;
   }
};

export default meetingReducer;
