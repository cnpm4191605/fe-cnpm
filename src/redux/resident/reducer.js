import actionTypes from "./actionTypes.js";
const initialState = {
   residents: [],
   resident: {},
   limit: 20,
   offset: 1,
   keyword: "",
   minAge: 0,
   maxAge: 150,
   gender: "",
   status: "",
   totalRecords: 0,
   residentIdSelected: -1,
};

const residentReducer = (state = initialState, action) => {
   switch (action.type) {
      case actionTypes.GET_LIST_RESIDENT_SUCCESS:
         return {
            ...state,
            residents: [...action.payload.items],
            totalRecords: action.payload.totalItems,
         };
      case actionTypes.GET_LIST_RESIDENT_FAIL:
         return {
            ...state,
            residents: [],
            totalRecords: 0,
         };

      // set danh sách tham số paging and filter
      case actionTypes.PAGING_FILTER:
         console.log("page:", action.payload);
         return {
            ...state,
            offset: action.payload.offset,
            limit: action.payload.limit,
            keyword: action.payload.keyword,
            minAge: action.payload.minAge,
            maxAge: action.payload.maxAge,
            gender: action.payload.gender,
            status: action.payload.status,
         };

      // xóa nhân khẩu
      case actionTypes.DELETE_RESIDENT_SUCCESS:
         const arrayDelete = [...state.residents];
         let idx = arrayDelete.findIndex(
            (resident) => resident.id === action.payload
         );
         arrayDelete.splice(idx, 1);
         if (arrayDelete.length === 0) {
            return {
               ...state,
               offset: state.offset === 1 ? 1 : state.offset - 1,
               totalRecords: state.totalRecords - 1,
               residents: [...arrayDelete],
            };
         } else {
            return {
               ...state,
               totalRecords: state.totalRecords - 1,
               residents: [...arrayDelete],
            };
         }

      // filter resident by keyword
      case actionTypes.GET_RESIDENT_FILTER_SUCCESS:
         return {
            ...state,
            residents: [...action.payload],
         };
      case actionTypes.GET_RESIDENT_FILTER_FAIL:
         return {
            ...state,
            residents: [],
         };

      // create resident
      case actionTypes.CREATE_RESIDENT_SUCCESS:
         return {
            ...state,
            totalRecords: state.totalRecords + 1,
            // resident: { ...action.payload },
            residents: [{ ...action.payload }, ...state.residents],
         };

      // update househld
      case actionTypes.UPDATE_RESIDENT_SUCCESS:
         const arrayUpdate = [...state.residents];
         let idxUpdate = arrayUpdate.findIndex(
            (resident) => resident.id === action.payload.id
         );
         if (idxUpdate !== -1) {
            arrayUpdate[idxUpdate] = { ...action.payload };
            return {
               ...state,
               resident: {},
               residents: [...arrayUpdate],
            };
         }
         return state;

      // toggle modal
      case actionTypes.TOGGLE_MODAL:
         return {
            ...state,
            // resident: {},
            typeModal: action.payload.typeModal,
            openModal: action.payload.openModal,
         };
      // toggle modal
      case actionTypes.TOGGLE_DECLARATION:
         return {
            ...state,
            // resident: {},
            typeDeclaration: action.payload.typeDeclaration,
            openDeclaration: action.payload.openDeclaration,
         };

      case actionTypes.GET_RESIDENT_BY_ID_SUCCESS:
         return {
            ...state,
            resident: { ...action.payload },
         };
      case actionTypes.GET_RESIDENT_BY_ID_FAIL:
         return {
            ...state,
            resident: {},
         };
      case actionTypes.SET_RESIDENT_ID_SELECT:
         return {
            ...state,
            residentIdSelected: action.payload,
         };
      default:
         return state;
   }
};

export default residentReducer;
