import { combineReducers, createStore, applyMiddleware } from "redux";
import userReducer from "./users/reducer";
import thunk from "redux-thunk";
import householdReducer from "./household/reducer";
import commonReducer from "./common/reducer";
import { localStore } from "utils/constants";
import residentReducer from "./resident/reducer";
import meetingReducer from "./meeting/reducer";
const rootReducer = combineReducers({
   user: userReducer,
   household: householdReducer,
   common: commonReducer,
   resident: residentReducer,
   meeting: meetingReducer,
});
const user = {
   userInfo: localStorage.getItem(localStore.USER)
      ? JSON.parse(localStorage.getItem(localStore.USER))
      : null,
   token: localStorage.getItem(localStore.TOKEN)
      ? JSON.parse(localStorage.getItem(localStore.TOKEN))
      : null,
};
console.log(user);
const initState = {
   user: user,
};
const store = createStore(rootReducer, initState, applyMiddleware(thunk));
export default store;
