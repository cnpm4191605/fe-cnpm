import Pagination from "react-bootstrap/Pagination";
import classNames from "classnames/bind";
import style from "./Paging.module.scss";
import { Dropdown, DropdownButton } from "react-bootstrap";
import React from "react";
const cx = classNames.bind(style);
function BasePaging({
   limit,
   offset,
   totalRecords,
   onChangePage,
   onChangeLimit,
}) {
   const totalPages =
      totalRecords % limit
         ? Math.floor(totalRecords / limit) + 1
         : Math.floor(totalRecords / limit);
   return (
      <div className={cx("wrapper")}>
         <div className={cx("paging-top")}>
            {totalRecords > 0 ? (
               <Pagination>
                  <DropdownButton
                     id="dropdown-basic-button"
                     title={`${limit} bản ghi trên 1 trang`}
                     className={cx("me-auto")}
                  >
                     <Dropdown.Item
                        as="button"
                        onClick={() => onChangeLimit(10)}
                        active={limit === 10}
                     >
                        10
                     </Dropdown.Item>
                     <Dropdown.Item
                        as="button"
                        onClick={() => onChangeLimit(20)}
                        active={limit === 20}
                     >
                        20
                     </Dropdown.Item>
                     <Dropdown.Item
                        as="button"
                        onClick={() => onChangeLimit(50)}
                        active={limit === 50}
                     >
                        50
                     </Dropdown.Item>
                     <Dropdown.Item
                        as="button"
                        onClick={() => onChangeLimit(100)}
                        active={limit === 100}
                     >
                        100
                     </Dropdown.Item>
                  </DropdownButton>

                  <Pagination.First
                     disabled={offset === 1}
                     onClick={() => onChangePage(1)}
                  />
                  <Pagination.Prev
                     disabled={offset === 1}
                     onClick={() => onChangePage(offset - 1)}
                  />
                  {totalPages <= 6 ? (
                     <>
                        {Array.from(Array(totalPages), (e, i) => (
                           <Pagination.Item
                              key={i}
                              active={offset === i + 1}
                              onClick={() => onChangePage(i + 1)}
                           >
                              {i + 1}
                           </Pagination.Item>
                        ))}
                     </>
                  ) : (
                     <>
                        {offset === 1 || offset === 2 || offset === 3 ? (
                           <>
                              <Pagination.Item
                                 active={offset === 1}
                                 onClick={() => onChangePage(1)}
                              >
                                 {1}
                              </Pagination.Item>
                              <Pagination.Item
                                 active={offset === 2}
                                 onClick={() => onChangePage(2)}
                              >
                                 {2}
                              </Pagination.Item>
                              <Pagination.Item
                                 active={offset === 3}
                                 onClick={() => onChangePage(3)}
                              >
                                 {3}
                              </Pagination.Item>
                              <Pagination.Item onClick={() => onChangePage(4)}>
                                 {4}
                              </Pagination.Item>
                              <Pagination.Ellipsis />
                              <Pagination.Item
                                 onClick={() => onChangePage(totalPages)}
                              >
                                 {totalPages}
                              </Pagination.Item>
                           </>
                        ) : (
                           <>
                              {offset === totalPages ||
                              offset === totalPages - 1 ||
                              offset === totalPages - 2 ? (
                                 <>
                                    <Pagination.Item
                                       onClick={() => onChangePage(1)}
                                    >
                                       {1}
                                    </Pagination.Item>
                                    <Pagination.Ellipsis />
                                    <Pagination.Item
                                       onClick={() =>
                                          onChangePage(totalPages - 3)
                                       }
                                    >
                                       {totalPages - 3}
                                    </Pagination.Item>
                                    <Pagination.Item
                                       active={offset === totalPages - 2}
                                       onClick={() =>
                                          onChangePage(totalPages - 2)
                                       }
                                    >
                                       {totalPages - 2}
                                    </Pagination.Item>
                                    <Pagination.Item
                                       active={offset === totalPages - 1}
                                       onClick={() =>
                                          onChangePage(totalPages - 1)
                                       }
                                    >
                                       {totalPages - 1}
                                    </Pagination.Item>
                                    <Pagination.Item
                                       active={offset === totalPages}
                                       onClick={() => onChangePage(totalPages)}
                                    >
                                       {totalPages}
                                    </Pagination.Item>
                                 </>
                              ) : (
                                 <>
                                    <Pagination.Item
                                       onClick={() => onChangePage(1)}
                                    >
                                       {1}
                                    </Pagination.Item>
                                    <Pagination.Ellipsis />
                                    <Pagination.Item
                                       onClick={() => onChangePage(offset - 1)}
                                    >
                                       {offset - 1}
                                    </Pagination.Item>
                                    <Pagination.Item
                                       active
                                       onClick={() => onChangePage(offset)}
                                    >
                                       {offset}
                                    </Pagination.Item>
                                    <Pagination.Item
                                       onClick={() => onChangePage(offset + 1)}
                                    >
                                       {offset + 1}
                                    </Pagination.Item>
                                    <Pagination.Ellipsis />
                                    <Pagination.Item
                                       onClick={() => onChangePage(totalPages)}
                                    >
                                       {totalPages}
                                    </Pagination.Item>
                                 </>
                              )}
                           </>
                        )}
                     </>
                  )}
                  <Pagination.Next
                     disabled={offset === totalPages}
                     onClick={() => onChangePage(offset + 1)}
                  />
                  <Pagination.Last
                     disabled={offset === totalPages}
                     onClick={() => onChangePage(totalPages)}
                  />
               </Pagination>
            ) : (
               <></>
            )}
         </div>
         <div className={cx("paging-bottom")}>
            Tổng:&nbsp;
            <b>{totalRecords}</b>
            &nbsp;bản ghi
         </div>
      </div>
   );
}

export default React.memo(BasePaging);
