import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { localStore, path } from "utils/constants.js";
import Login from "pages/Login/Login.jsx";
import HomePage from "pages/HomePage/HomePage";
import NotFound from "pages/NotFound/NotFound";
import DefaultLayout from "layouts/DefaultLayout/DefaultLayout";
import HouseholdPage from "pages/HouseholdPage/HouseholdPage";
import Loading from "components/Loading/Loading";
import "./App.scss";
import ResidentPage from "pages/ResidentPage/ResidentPage";
import MeetingPage from "pages/MeetingPage/MeetingPage";
import HouseholdDetailPage from "pages/HouseholdDetailPage/HouseholdDetailPage";
import { useEffect } from "react";
import MeetingDetailPage from "pages/MeetingDetailPage/MeetingDetailPage";
function App() {
   const token = useSelector((state) => state.user.token);
   const loading = useSelector((state) => state.common.loading);
   // useEffect(() => {
   //    const handleTabClose = (event) => {
   //       localStorage.removeItem(localStore.USER);
   //       localStorage.removeItem(localStore.TOKEN);
   //    };

   //    window.addEventListener("beforeunload", handleTabClose);

   //    return () => {
   //       window.removeEventListener("beforeunload", handleTabClose);
   //    };
   // }, []);
   return (
      <BrowserRouter>
         {!token ? (
            <Routes>
               <Route path={path.LOGIN} element={<Login />} />
               <Route path="*" element={<Navigate to={path.LOGIN} />} />
            </Routes>
         ) : (
            <Routes>
               <Route path="/" element={<DefaultLayout />}>
                  <Route
                     path={path.LOGIN}
                     element={<Navigate to={path.HOME} />}
                  />
                  <Route path={path.HOME} element={<HomePage />} />
                  <Route path={path.HOUSEHOLD} element={<HouseholdPage />} />
                  <Route path={path.RESIDENT} element={<ResidentPage />} />
                  <Route path={path.MEETING} element={<MeetingPage />} />

                  <Route
                     path={path.HOUSEHOLD_DETAIL}
                     element={<HouseholdDetailPage />}
                  />
                  <Route
                     path={path.MEETING_DETAIL}
                     element={<MeetingDetailPage />}
                  />
                  <Route path="*" element={<NotFound />} />
               </Route>
            </Routes>
         )}
         <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="light"
         />
         {loading && <Loading />}
      </BrowserRouter>
   );
}

export default App;
