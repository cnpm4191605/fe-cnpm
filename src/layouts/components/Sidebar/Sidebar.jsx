import { NavLink } from "react-router-dom";
import classNames from "classnames/bind";
import { path } from "utils/constants";
import styles from "./Sidebar.module.scss";
let cx = classNames.bind(styles);
const sidebarNavItems = [
   {
      display: "Dashboard",
      icon: <i className="bx bx-home"></i>,
      to: "/",
      section: "",
   },
   {
      display: "Getting Started",
      icon: <i className="bx bx-star"></i>,
      to: "/started",
      section: "started",
   },
   {
      display: "Calendar",
      icon: <i className="bx bx-calendar"></i>,
      to: "/calendar",
      section: "calendar",
   },
   {
      display: "User",
      icon: <i className="bx bx-user"></i>,
      to: "/user",
      section: "user",
   },
   {
      display: "Orders",
      icon: <i className="bx bx-receipt"></i>,
      to: "/order",
      section: "order",
   },
];
function Sidebar() {
   return (
      <div className={cx("wrapper")}>
         <ul className={cx("sidebar")}>
            <NavLink
               to={path.HOME}
               className={({ isActive }) => (isActive ? cx("active") : "")}
            >
               <li className={cx("sidebar-item")}>Trang chủ</li>
            </NavLink>
            <NavLink
               to={path.HOUSEHOLD}
               className={({ isActive }) => (isActive ? cx("active") : "")}
            >
               <li className={cx("sidebar-item")}>Hộ khẩu</li>
            </NavLink>
            <NavLink
               to={path.RESIDENT}
               className={({ isActive }) => (isActive ? cx("active") : "")}
            >
               <li className={cx("sidebar-item")}>Nhân khẩu</li>
            </NavLink>
            <NavLink
               to={path.MEETING}
               className={({ isActive }) => (isActive ? cx("active") : "")}
            >
               <li className={cx("sidebar-item")}>Cuộc họp</li>
            </NavLink>
         </ul>
      </div>
   );
}
export default Sidebar;
