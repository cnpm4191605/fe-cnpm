import classNames from "classnames/bind";
import Header from "layouts/components/Header/Header";
import Navbar from "layouts/components/Navbar/Navbar";
import Sidebar from "layouts/components/Sidebar/Sidebar";
import styles from "./DefaultLayout.module.scss";
import { Outlet } from "react-router-dom";
import { Row, Col, Container } from "react-bootstrap";
let cx = classNames.bind(styles);
function DefaultLayout() {
   return (
      <div className={cx("wrapper")}>
         <Header />
         <div className={cx("body")}>
            <Row>
               <Col sm={2} className={cx("height-body")}>
                  <Sidebar />
               </Col>
               <Col sm={10} className={cx("height-body")}>
                  <Outlet />
               </Col>
            </Row>
         </div>
      </div>
   );
}
export default DefaultLayout;
